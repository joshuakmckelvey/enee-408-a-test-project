
// Definitions for Major Processor Opcodes
`define LOAD        5'b00000
`define LOAD_FP     5'b00001
`define CSTM_0      5'b00010
`define MSC_MEM     5'b00011
`define OP_IMM      5'b00100
`define AUIPC       5'b00101
`define OP_IMM32    5'b00110
`define IW48_0      5'b00111
`define STORE       5'b01000
`define STORE_FP    5'b01001
`define CSTM_1      5'b01010
`define AMO         5'b01011
`define OP          5'b01100
`define LUI         5'b01101
`define OP_32       5'b01110
`define IW64        5'b01111
`define MADD        5'b10000
`define MSUB        5'b10001
`define NMSUB       5'b10010
`define NMADD       5'b10011
`define OP_FP       5'b10100
`define RESERV_0    5'b10101
`define CSTM_2      5'b10110
`define IW48_1      5'b10111
`define BRANCH      5'b11000
`define JALR        5'b11001
`define RESERV_1    5'b11010
`define JAL         5'b11011
`define SYSTEM      5'b11100
`define RESERV_2    5'b11101
`define CSTM_3      5'b11110
`define IW_80       5'b11111

// Definitions for minor opcode fields

// funct3 Fields

`define F3_JALR 3'b000

`define F3_EQ   3'b000
`define F3_NE   3'b001
`define F3_LT   3'b100
`define F3_GE   3'b101
`define F3_LTU  3'b110
`define F3_GEU  3'b111

`define F3_LB   3'b000
`define F3_LH   3'b001
`define F3_LW   3'b010
`define F3_LBU  3'b100
`define F3_LHU  3'b101

`define F3_SB   3'b000
`define F3_SH   3'b001
`define F3_SW   3'b010

`define F3_ADDI 3'b000
`define F3_SLTI 3'b010
`define F3_SLTIU 3'b011
`define F3_XORI 3'b100
`define F3_ORI  3'b110
`define F3_ANDI 3'b111

`define F3_SLLI 3'b001
`define F3_SRI  3'b101

`define F3_ADD  3'b000
`define F3_SUB  3'b000
`define F3_SLL  3'b001
`define F3_SLT  3'b010
`define F3_SLTU 3'b011
`define F3_XOR  3'b100
`define F3_SR   3'b101
`define F3_OR   3'b110
`define F3_AND  3'b111

`define F3_FENCE    3'b000
`define F3_EACT     3'b000 // Environment Action (ECALL/EBREAK)
`define F3_CSRRW    3'b001
`define F3_CSRRS    3'b010
`define F3_CSRRC    3'b011
`define F3_CSRRWI   3'b101
`define F3_CSRRSI   3'b110
`define F3_CSRRCI   3'b111

// RS2 Function fields

`define RS2_ECALL   5'b00000
`define RS2_EBREAK  5'b00001
`define RS2_RET     5'b00010

// funct7 Fields

//`define F7_SLLI 7'b0000000
`define F7_SRLI 7'b0000000
`define F7_SRAI 7'b0100000
`define F7_ADD  7'b0000000
`define F7_SUB  7'b0100000
//`define F7_SLL  7'b0000000
`define F7_SLT  7'b0000000
`define F7_SLTU 7'b0000000
`define F7_XOR  7'b0000000
`define F7_SRL  7'b0000000
`define F7_SRA  7'b0100000
`define F7_OR   7'b0000000
`define F7_AND  7'b0000000

`define F7_URET 7'b0000000
`define F7_SRET 7'b0001000
`define F7_MRET 7'b0011000

`define UMODE   2'b00
`define SMODE   2'b01
`define MMODE   2'b11

`define PROD_RF 2'b00
`define PROD_NA 2'b00
`define PROD_EX 2'b01
`define PROD_MA 2'b10

`define INST_OP     6:2   // major opcode
`define INST_F3     14:12 // minor F3 opcode
`define INST_F7     31:25 // minor F7 opcode

`define INST_RD     11:7  // rd
`define INST_RS1    19:15 // rs1
`define INST_RS2    24:20 // rs2


`define INP_SEL1_S1   1'b0
`define INP_SEL1_PC   1'b1
`define INP_SEL2_S2   1'b0
`define INP_SEL2_IMM  1'b1

`define OUT_SEL_ALU   2'b00
`define OUT_SEL_IMM   2'b01
`define OUT_SEL_PC4   2'b10
`define OUT_SEL_CSR   2'b11

`define ECAUSE_NONE   3'b000
`define ECAUSE_ECALL  3'b001
`define ECAUSE_MRET   3'b010
`define ECAUSE_INV    3'b111

`define F7E_STND     2'b00   // Standard ALU OP
`define F7E_SUB_A    2'b01   // Subtract or arithmetic shift
//`define `F7E_MUL_DIV  2'b10   // RV32M Instructions
`define F7E_ADD      2'b11   // Add only (For Address computation)

module inst_dec (inst, priv, csr_val, cmp_op, jmp_op, ld_op, st_op, csr_op,
                 inp_sel1, inp_sel2, out_sel, ecause, prod, rs1_fw, rs2_fw, 
                 f3, f7e, rd);
  
  input [31:0] inst;
  input [1:0] priv;   // Current priv
  input csr_val;      // Decoded CSR is valid

  output reg cmp_op, jmp_op, ld_op, st_op, csr_op;
  output reg inp_sel1, inp_sel2; // Selects ALU inputs
  output reg [1:0] out_sel; // Selects EX stage result output
  output reg [2:0] ecause;
  output reg [1:0] prod;
  output reg rs1_fw, rs2_fw;
  output reg [2:0] f3;
  output reg [1:0] f7e; // F7 field encoded
  output reg [4:0] rd;

  always @(*) begin
    case (inst[`INST_OP])
    // RV32I, Zifencei, Zicsr, RV32M
    `LUI: begin
      cmp_op    = 1'b0;
      jmp_op    = 1'b0;
      ld_op     = 1'b0;
      st_op     = 1'b0;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_S1;   // X
      inp_sel2  = `INP_SEL2_IMM;  // X
      out_sel   = `OUT_SEL_IMM;
      ecause    = `ECAUSE_NONE;
      prod      = `PROD_EX;
      rs1_fw    = 1'b0;
      rs2_fw    = 1'b0;
      f3        = inst[`INST_F3];
      f7e       = `F7E_STND;  // X
      rd        = inst[`INST_RD];
    end
    `AUIPC: begin
      cmp_op    = 1'b0;
      jmp_op    = 1'b0;
      ld_op     = 1'b0;
      st_op     = 1'b0;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_PC;
      inp_sel2  = `INP_SEL2_IMM;
      out_sel   = `OUT_SEL_ALU;
      ecause    = `ECAUSE_NONE;
      prod      = `PROD_EX;
      rs1_fw    = 1'b0;
      rs2_fw    = 1'b0;
      f3        = inst[`INST_F3];
      f7e       = `F7E_ADD;
      rd        = inst[`INST_RD];
    end
    `JAL: begin
      cmp_op    = 1'b0;
      jmp_op    = 1'b1;
      ld_op     = 1'b0;
      st_op     = 1'b0;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_PC;
      inp_sel2  = `INP_SEL2_IMM;
      out_sel   = `OUT_SEL_PC4;
      ecause    = `ECAUSE_NONE;
      prod      = `PROD_EX;
      rs1_fw    = 1'b0;
      rs2_fw    = 1'b0;
      f3        = inst[`INST_F3];
      f7e       = `F7E_ADD;
      rd        = inst[`INST_RD];
    end
    `JALR: begin
      cmp_op    = 1'b0;
      jmp_op    = 1'b1;
      ld_op     = 1'b0;
      st_op     = 1'b0;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_S1;
      inp_sel2  = `INP_SEL2_IMM;
      out_sel   = `OUT_SEL_PC4;
      ecause    = `ECAUSE_NONE;
      prod      = `PROD_EX;
      rs1_fw    = 1'b1;
      rs2_fw    = 1'b0;
      f3        = inst[`INST_F3];
      f7e       = `F7E_ADD;
      rd        = inst[`INST_RD];
    end
    `BRANCH: begin
      cmp_op    = 1'b1;
      jmp_op    = 1'b0;
      ld_op     = 1'b0;
      st_op     = 1'b0;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_PC;
      inp_sel2  = `INP_SEL2_IMM;
      out_sel   = `OUT_SEL_PC4; // X
      ecause    = `ECAUSE_NONE;
      prod      = `PROD_NA;
      rs1_fw    = 1'b1;
      rs2_fw    = 1'b1;
      f3        = inst[`INST_F3];
      f7e       = `F7E_ADD;
      rd        = 5'b00000;
    end
    `LOAD: begin
      cmp_op    = 1'b0;
      jmp_op    = 1'b0;
      ld_op     = 1'b1;
      st_op     = 1'b0;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_S1;
      inp_sel2  = `INP_SEL2_IMM;
      out_sel   = `OUT_SEL_ALU;
      ecause    = `ECAUSE_NONE;
      prod      = `PROD_MA;
      rs1_fw    = 1'b1;
      rs2_fw    = 1'b0;
      f3        = inst[`INST_F3];
      f7e       = `F7E_ADD;
      rd        = inst[`INST_RD];
                                    // ADD DAG Align control
    end
    `STORE: begin
      cmp_op    = 1'b0;
      jmp_op    = 1'b0;
      ld_op     = 1'b0;
      st_op     = 1'b1;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_S1;
      inp_sel2  = `INP_SEL2_IMM;
      out_sel   = `OUT_SEL_ALU;
      ecause    = `ECAUSE_NONE;
      prod      = `PROD_NA;         // ??
      rs1_fw    = 1'b1;
      rs2_fw    = 1'b1;
      f3        = inst[`INST_F3];
      f7e       = `F7E_ADD;
      rd        = 5'b00000;
    end
    `OP_IMM: begin
      cmp_op    = 1'b0;
      jmp_op    = 1'b0;
      ld_op     = 1'b0;
      st_op     = 1'b0;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_S1;
      inp_sel2  = `INP_SEL2_IMM;
      out_sel   = `OUT_SEL_ALU;
      ecause    = `ECAUSE_NONE;
      prod      = `PROD_EX;
      rs1_fw    = 1'b1;
      rs2_fw    = 1'b0;
      f3        = inst[`INST_F3];
      if (inst[`INST_F3] == `F3_SRI && inst[`INST_F7] == `F7_SRAI)
        f7e       = `F7E_SUB_A;
      else
        f7e       = `F7E_STND;
      rd        = inst[`INST_RD];
    end
    `OP: begin
      cmp_op    = 1'b0;
      jmp_op    = 1'b0;
      ld_op     = 1'b0;
      st_op     = 1'b0;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_S1;
      inp_sel2  = `INP_SEL2_S2;
      out_sel   = `OUT_SEL_ALU;
      ecause    = `ECAUSE_NONE;
      prod      = `PROD_EX;
      rs1_fw    = 1'b1;
      rs2_fw    = 1'b1;
      f3        = inst[`INST_F3];
      if (inst[`INST_F7] == `F7_SRA)  // Instruction is SRA or SUB
        f7e       = `F7E_SUB_A;
      else
        f7e       = `F7E_STND;
      rd        = inst[`INST_RD];
    end
    `MSC_MEM: begin             // Fence (No-OP)
      cmp_op    = 1'b0;
      jmp_op    = 1'b0;
      ld_op     = 1'b0;
      st_op     = 1'b0;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_S1; // X
      inp_sel2  = `INP_SEL2_S2; // X
      out_sel   = `OUT_SEL_ALU; // X
      ecause    = `ECAUSE_NONE;
      prod      = `PROD_NA;
      rs1_fw    = 1'b0;
      rs2_fw    = 1'b0;
      f3        = inst[`INST_F3];
      f7e       = `F7E_STND;    // X
      rd        = 5'b00000;     // X
    end
    `SYSTEM: begin
      case (inst[`INST_F3])
        `F3_EACT: begin
          case (inst[`INST_RS2])
            `RS2_ECALL: begin
              cmp_op    = 1'b0;
              jmp_op    = 1'b0;
              ld_op     = 1'b0;
              st_op     = 1'b0;
              csr_op    = 1'b0;
              inp_sel1  = `INP_SEL1_S1; // X
              inp_sel2  = `INP_SEL2_S2; // X
              out_sel   = `OUT_SEL_ALU; // X
              ecause    = `ECAUSE_ECALL;
              prod      = `PROD_NA;
              rs1_fw    = 1'b0;
              rs2_fw    = 1'b0;
              f3        = inst[`INST_F3];
              f7e       = `F7E_STND;    // X
              rd        = 5'b00000;     // X
            end
            `RS2_EBREAK: begin          // Invalid/Not inplemented
              cmp_op    = 1'b0;
              jmp_op    = 1'b0;
              ld_op     = 1'b0;
              st_op     = 1'b0;
              csr_op    = 1'b0;
              inp_sel1  = `INP_SEL1_S1; // X
              inp_sel2  = `INP_SEL2_S2; // X
              out_sel   = `OUT_SEL_ALU; // X
              ecause    = `ECAUSE_INV;
              prod      = `PROD_NA;
              rs1_fw    = 1'b0;
              rs2_fw    = 1'b0;
              f3        = inst[`INST_F3];
              f7e       = `F7E_STND;    // X
              rd        = 5'b00000;     // X
            end
            `RS2_RET: begin
              case (inst[`INST_F7])
                `F7_URET: begin         // Invalid/Not implemented
                  cmp_op    = 1'b0;
                  jmp_op    = 1'b0;
                  ld_op     = 1'b0;
                  st_op     = 1'b0;
                  csr_op    = 1'b0;
                  inp_sel1  = `INP_SEL1_S1; // X
                  inp_sel2  = `INP_SEL2_S2; // X
                  out_sel   = `OUT_SEL_ALU; // X
                  ecause    = `ECAUSE_INV;
                  prod      = `PROD_NA;
                  rs1_fw    = 1'b0;
                  rs2_fw    = 1'b0;
                  f3        = inst[`INST_F3];
                  f7e       = `F7E_STND;    // X
                  rd        = 5'b00000;     // X
                end
                `F7_SRET: begin         // Invalid/Not implemented
                  cmp_op    = 1'b0;
                  jmp_op    = 1'b0;
                  ld_op     = 1'b0;
                  st_op     = 1'b0;
                  csr_op    = 1'b0;
                  inp_sel1  = `INP_SEL1_S1; // X
                  inp_sel2  = `INP_SEL2_S2; // X
                  out_sel   = `OUT_SEL_ALU; // X
                  ecause    = `ECAUSE_INV;
                  prod      = `PROD_NA;
                  rs1_fw    = 1'b0;
                  rs2_fw    = 1'b0;
                  f3        = inst[`INST_F3];
                  f7e       = `F7E_STND;    // X
                  rd        = 5'b00000;     // X
                end
                `F7_MRET: if (priv == `MMODE) begin
                  cmp_op    = 1'b0;
                  jmp_op    = 1'b0;
                  ld_op     = 1'b0;
                  st_op     = 1'b0;
                  csr_op    = 1'b0;
                  inp_sel1  = `INP_SEL1_S1; // X
                  inp_sel2  = `INP_SEL2_S2; // X
                  out_sel   = `OUT_SEL_ALU; // X
                  ecause    = `ECAUSE_MRET;
                  prod      = `PROD_NA;
                  rs1_fw    = 1'b0;
                  rs2_fw    = 1'b0;
                  f3        = inst[`INST_F3];
                  f7e       = `F7E_STND;    // X
                  rd        = 5'b00000;     // X
                  end else begin  // Not Correct Mode (Invalid)
                  cmp_op    = 1'b0;
                  jmp_op    = 1'b0;
                  ld_op     = 1'b0;
                  st_op     = 1'b0;
                  csr_op    = 1'b0;
                  inp_sel1  = `INP_SEL1_S1; // X
                  inp_sel2  = `INP_SEL2_S2; // X
                  out_sel   = `OUT_SEL_ALU; // X
                  ecause    = `ECAUSE_INV;
                  prod      = `PROD_NA;
                  rs1_fw    = 1'b0;
                  rs2_fw    = 1'b0;
                  f3        = inst[`INST_F3];
                  f7e       = `F7E_STND;    // X
                  rd        = 5'b00000;     // X
                end
                default: begin         // Invalid Instruction
                  cmp_op    = 1'b0;
                  jmp_op    = 1'b0;
                  ld_op     = 1'b0;
                  st_op     = 1'b0;
                  csr_op    = 1'b0;
                  inp_sel1  = `INP_SEL1_S1; // X
                  inp_sel2  = `INP_SEL2_S2; // X
                  out_sel   = `OUT_SEL_ALU; // X
                  ecause    = `ECAUSE_INV;
                  prod      = `PROD_NA;
                  rs1_fw    = 1'b0;
                  rs2_fw    = 1'b0;
                  f3        = inst[`INST_F3];
                  f7e       = `F7E_STND;    // X
                  rd        = 5'b00000;     // X
                end
              endcase
            end
            default: begin         // Invalid Instruction
              cmp_op    = 1'b0;
              jmp_op    = 1'b0;
              ld_op     = 1'b0;
              st_op     = 1'b0;
              csr_op    = 1'b0;
              inp_sel1  = `INP_SEL1_S1; // X
              inp_sel2  = `INP_SEL2_S2; // X
              out_sel   = `OUT_SEL_ALU; // X
              ecause    = `ECAUSE_INV;
              prod      = `PROD_NA;
              rs1_fw    = 1'b0;
              rs2_fw    = 1'b0;
              f3        = inst[`INST_F3];
              f7e       = `F7E_STND;    // X
              rd        = 5'b00000;     // X
            end
          endcase
        end
        `F3_CSRRW, `F3_CSRRS, `F3_CSRRC,`F3_CSRRWI, `F3_CSRRSI, `F3_CSRRCI: begin
          if (priv == `MMODE && csr_val) begin
            cmp_op    = 1'b0;
            jmp_op    = 1'b0;
            ld_op     = 1'b0;
            st_op     = 1'b0;
            csr_op    = 1'b1;
            inp_sel1  = `INP_SEL1_S1;   // X
            inp_sel2  = `INP_SEL2_S2;   // X
            out_sel   = `OUT_SEL_CSR;
            ecause    = `ECAUSE_NONE;
            prod      = `PROD_NA;
            rs1_fw    = 1'b1;
            rs2_fw    = 1'b0;
            f3        = inst[`INST_F3];
            f7e       = `F7E_STND;    // X
            rd        = inst[`INST_RD];
          end else begin    // Invalid Priv or CSR
            cmp_op    = 1'b0;
            jmp_op    = 1'b0;
            ld_op     = 1'b0;
            st_op     = 1'b0;
            csr_op    = 1'b0;
            inp_sel1  = `INP_SEL1_S1; // X
            inp_sel2  = `INP_SEL2_S2; // X
            out_sel   = `OUT_SEL_ALU; // X
            ecause    = `ECAUSE_INV;
            prod      = `PROD_NA;
            rs1_fw    = 1'b0;
            rs2_fw    = 1'b0;
            f3        = inst[`INST_F3];
            f7e       = `F7E_STND;    // X
            rd        = 5'b00000;     // X
          end
        end
        default: begin         // Invalid Instruction
          cmp_op    = 1'b0;
          jmp_op    = 1'b0;
          ld_op     = 1'b0;
          st_op     = 1'b0;
          csr_op    = 1'b0;
          inp_sel1  = `INP_SEL1_S1; // X
          inp_sel2  = `INP_SEL2_S2; // X
          out_sel   = `OUT_SEL_ALU; // X
          ecause    = `ECAUSE_INV;
          prod      = `PROD_NA;
          rs1_fw    = 1'b0;
          rs2_fw    = 1'b0;
          f3        = inst[`INST_F3];
          f7e       = `F7E_STND;    // X
          rd        = 5'b00000;     // X
        end
      endcase
    end
    default: begin         // Invalid Instruction
      cmp_op    = 1'b0;
      jmp_op    = 1'b0;
      ld_op     = 1'b0;
      st_op     = 1'b0;
      csr_op    = 1'b0;
      inp_sel1  = `INP_SEL1_S1; // X
      inp_sel2  = `INP_SEL2_S2; // X
      out_sel   = `OUT_SEL_ALU; // X
      ecause    = `ECAUSE_INV;
      prod      = `PROD_NA;
      rs1_fw    = 1'b0;
      rs2_fw    = 1'b0;
      f3        = inst[`INST_F3];
      f7e       = `F7E_STND;    // X
      rd        = 5'b00000;     // X
    end
    endcase
  end

endmodule
