// Frame buffer memory
// Port 0 is read/write, port 1 is read only

// Contains definitions for vga mode and resolution
`include "disp_config.vh"

module fb_mem (

  input clk_i, en_i,
  input [31:0] data_i,
  input [31:0] addr_i_0, addr_i_1,
  input [3:0]  be_i,

  input dir_i,

  output reg val_o,

  output [31:0] data_o_0, data_o_1

);

  parameter FILE = "";
  parameter ADDR_WIDTH = 13;

  parameter V_ADDR_HIGH = (`V_BITS_ACT+`H_BITS_ACT-1);
  parameter V_ADDR_LOW  = `H_BITS_ACT + `V_BITS_TRUNC;
  parameter H_ADDR_HIGH = (`H_BITS_ACT-1);
  parameter H_ADDR_LOW  = `H_BITS_TRUNC;

  // CPU Side
  mem       #(.FILE     (FILE),
              .ADDR_WIDTH(ADDR_WIDTH))
        FB_CPU(.clk_i    (clk_i),
              .en_i     (en_i),
              .data_i   (data_i),
              .wr_addr_i(addr_i_0),
              .rd_addr_i(addr_i_0),
              .dir_i    (dir_i),
              .val_i    (be_i),
              .data_o   (data_o_0));

  // VGA Side
  mem       #(.FILE     (FILE),
              .ADDR_WIDTH(ADDR_WIDTH))
       FB_VGA(.clk_i    (clk_i),
              .en_i     (en_i),
              .data_i   (data_i),
              .wr_addr_i(addr_i_0),
              .rd_addr_i({addr_i_1[V_ADDR_HIGH:V_ADDR_LOW],
                          addr_i_1[H_ADDR_HIGH:H_ADDR_LOW]}),
              //.rd_addr_i({addr_i_1[19:13], addr_i_1[9:3]}),
              //.rd_addr_i({addr_i_1[19:13], addr_i_1[6:2], addr_i_1[1:0]}),
              .dir_i    (dir_i),
              .val_i    (be_i),
              .data_o   (data_o_1));

  // Output is valid when accessed
  always @(posedge clk_i) begin
    
    if (en_i)
      val_o <= 1'b1;
    else
      val_o <= 1'b0;

  end

endmodule
