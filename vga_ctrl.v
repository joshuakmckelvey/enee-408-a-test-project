// VGA Controller

// Contains definitions for vga mode and resolution
`include "disp_config.vh"

module vga_ctrl (vga_clk, data_in, vga_addr, r_out, g_out, b_out, h_sync,
                 v_sync, blank, sync);

  input vga_clk;
  input [31:0] data_in;

  output reg [31:0] vga_addr;
  output reg [9:0] r_out, g_out, b_out;
  output reg h_sync, v_sync;
  output blank, sync;

  reg [`H_BITS-1:0] hcount;

  reg [`V_BITS-1:0] vcount;

  reg h_blank, v_blank;

  reg h_active_sig, h_fportch_sig, h_sync_sig, h_bportch_sig;

  reg v_active_sig, v_fportch_sig, v_sync_sig, v_bportch_sig;


  always @(posedge vga_clk) begin

    // Blank is active low
    if (blank) begin
      // Reads from little endian frame buffer
      if (hcount[(`H_BITS_TRUNC+1):`H_BITS_TRUNC] == 2'b00) begin
        r_out = {{data_in[31:24]}, 2'b00};
        g_out = {{data_in[31:24]}, 2'b00};
        b_out = {{data_in[31:24]}, 2'b00};
      end else if (hcount[(`H_BITS_TRUNC+1):`H_BITS_TRUNC] == 2'b01) begin
        r_out = {{data_in[23:16]}, 2'b00};
        g_out = {{data_in[23:16]}, 2'b00};
        b_out = {{data_in[23:16]}, 2'b00};
      end else if (hcount[(`H_BITS_TRUNC+1):`H_BITS_TRUNC] == 2'b10) begin
        r_out = {{data_in[15:8]}, 2'b00};
        g_out = {{data_in[15:8]}, 2'b00};
        b_out = {{data_in[15:8]}, 2'b00};
      end else begin
        r_out = {{data_in[7:0]}, 2'b00};
        g_out = {{data_in[7:0]}, 2'b00};
        b_out = {{data_in[7:0]}, 2'b00};
      end
    end else begin
      // Blanking time
      r_out = 10'b0000000000;
      g_out = 10'b0000000000;
      b_out = 10'b0000000000;
    end

    // Fetches next set to correct for delay
    if (hcount[(`H_BITS_TRUNC+1):1] == {(`H_BITS_TRUNC+1){1'b1}})
      vga_addr = {vcount[`V_BITS_ACT-1:0],hcount[`H_BITS_ACT-1:0]}+(1<<(`H_BITS_TRUNC+2));
    else
      vga_addr = {vcount[`V_BITS_ACT-1:0],hcount[`H_BITS_ACT-1:0]};

  end


  // Horizontal Timing signal generators
  always @(*) begin
    
    h_active_sig = hcount == `H_ACTIVE;
    h_fportch_sig = hcount == `H_FPORTCH;
    h_sync_sig = hcount == `H_SYNC;
    h_bportch_sig = hcount == `H_BPORTCH;
  
  end

  // Vertical Timing signal generators
  always @(*) begin
    
    v_active_sig = vcount == `V_ACTIVE;
    v_fportch_sig = vcount == `V_FPORTCH;
    v_sync_sig = vcount == `V_SYNC;
    v_bportch_sig = vcount == `V_BPORTCH;
  
  end

  // Display (~Blank) Latch
  always @(*) begin
    
    if (h_bportch_sig) begin
      h_blank <= 1'b1;
    end else if (h_active_sig) begin
      h_blank <= 1'b0;
    end
  
  end

  // H_Sync latch
  always @(*) begin
    
    if (h_fportch_sig) begin
      h_sync <= 1'b0;
    end else if (h_sync_sig) begin
      h_sync <= 1'b1;
    end
  
  end


  // VDisplay (~Blank) Latch
  always @(*) begin
    
    if (v_bportch_sig) begin
      v_blank <= 1'b1;
    end else if (v_active_sig) begin
      v_blank <= 1'b0;
    end
  
  end

  // V_Sync latch
  always @(*) begin
    
    if (v_fportch_sig) begin
      v_sync <= 1'b0;
    end else if (v_sync_sig) begin
      v_sync <= 1'b1;
    end
  
  end

  // Horizontal line counter
  always @(posedge vga_clk) begin
  
    if (h_bportch_sig)
      hcount <= 0;
    else
      hcount <= hcount + 1'b1;

  end

  // Vertical column counter
  always @(posedge vga_clk) begin
  
    if (v_bportch_sig)
      vcount <= 0;
    else if (h_bportch_sig) // Counts every line
      vcount <= vcount + 1'b1;

  end

  initial begin
  
    hcount = 0;
    vcount = 0;

  end

  assign blank = h_blank && v_blank;
  assign sync = 1'b0;

endmodule
