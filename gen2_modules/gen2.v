// Genesys 2 interface for kirv system
module gen2 (

  input sysclk_p,
  input sysclk_n,
  //input [1:0] KEY,
  output [7:0] led,
  //input [3:0] SW,

  // VGA
  output [4:0] vga_r, vga_b,
  output [5:0] vga_g,
  output vga_hs, vga_vs,

  // UART
  input uart_tx_in,
  output uart_rx_out

);

  wire single_step;

  wire clk_pll;

  wire [31:0] rled_reg;

  wire [7:0] vga_r_8b, vga_b_8b;
  wire [7:0] vga_g_8b;

  kirv_soc        #(.CLK_RATE   (25125000))
           SYSTEM  (.clk_i      (clk_pll),
                    .switch_i   (4'b0),
                    .key_i      (4'b1111),
                    .uart_rxd   (uart_tx_in), // !! Reversed because of uart dir
                    .uart_txd   (uart_rx_out),
                    .hex_disp   (),
                    .rled_reg_o (rled_reg),
                    .vga_r      (vga_r_8b),
                    .vga_g      (vga_g_8b),
                    .vga_b      (vga_b_8b),
                    .vga_hs     (vga_hs),
                    .vga_vs     (vga_vs),
                    .vga_blank  (),
                    .vga_sync   (),
                    .lcd_data_o (),
                    .lcd_ctrl_o ());

  // Clock PLL (200MHz to 10MHz)
  pll_200m_25p125m PLL (.clk_in1_n (sysclk_n),
                    .clk_in1_p (sysclk_p),
                    .clk_out1  (clk_pll));

  assign vga_r[4:0] = vga_r_8b[7:3];
  assign vga_g[5:0] = vga_g_8b[7:2];
  assign vga_b[4:0] = vga_b_8b[7:3];

  assign led[0]   = 1'b1;
  assign led[6:1] = 7'b0;
	
endmodule
