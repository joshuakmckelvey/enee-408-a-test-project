// DE0 interface for kirv system
module de0 (

  input CLOCK_50,
  input [1:0] KEY,
  output [7:0] LED,
  input [3:0] SW,
  input UART_RXD,
  output UART_TXD

);

  wire single_step;
  
  wire clk_pll;

  wire [31:0] rled_reg;

  kirv_soc SYSTEM (.clk_i     (clk_pll),
                   .switch_i  (SW[3:0]),
                   .key_i     ({2'b11, KEY}),
                   .uart_rxd  (UART_RXD),
                   .hex_disp  (),
                   .rled_reg_o(rled_reg),
                   .uart_txd  (UART_TXD),
                   .lcd_data_o(),
                   .lcd_ctrl_o());


  pll_50m_10m PLL (.inclk0    (CLOCK_50),
                   .c0        (clk_pll));

  assign LED[0]   = 1'b1;
  assign LED[6:1] = 7'b0;
	
endmodule
