// UART
module uart (

  input clk_i,
  input rxd_i,
  input ce_i,
  input func_i,
  input [7:0] data_i,

  output reg [31:0] data_o,
  output reg val_o,
  output reg int_o,
  output txd_o

);

  parameter REF_CLK = 10000000;
  parameter BAUDRATE = 9600;

  reg [7:0] tx_temp_buf = "Q";
  reg [7:0] rx_temp_buf = "A";

  // TX signals
  reg tx_start;
  wire tx_busy;

  // RX signals
  reg rx_clear;
  wire [7:0] rx_data;
  wire rx_full;

  reg tx_lock;    // Waiting for TX to start
  reg tx_wait;    // Waiting for TX to be ready
  reg rx_wait;    // Waiting for interrupt to be acknowledged
  reg rx_lock;    // Waiting for TX to be recieved

  initial begin
    tx_lock <= 1'b0;
    tx_wait <= 1'b0;
    rx_wait <= 1'b0;
    rx_lock <= 1'b0;
  end

  // Peripheral Controller
  always @(posedge clk_i) begin
    
    int_o <= 1'b0;
    tx_start <= 1'b0;
    val_o <= 1'b0;
    rx_clear <= 1'b0;
    
    // TX request
    if (ce_i && !func_i) begin

      tx_wait <= 1'b1;

    // RX Buffer Full, processor acknowledged
    end else if (((ce_i && func_i) || rx_lock) && rx_full && rx_wait) begin

      rx_clear <= 1'b1;
      rx_wait <= 1'b0;
      int_o <= 1'b0;
      val_o <= 1'b1;
      rx_lock <= 1'b0;

    // RX request, RX buffer empty
    end else if (ce_i && func_i) begin

      //rx_lock <= 1'b1;
      val_o <= 1'b1;
      data_o[7:0] <= 8'b0;


    // TX waiting for transmitter
    end else if (!ce_i && tx_busy && tx_wait) begin

      val_o <= 1'b0;

    // TX transmitter is now ready
    end else if (!tx_busy && tx_wait) begin

      tx_temp_buf <= data_i;
      tx_start <= 1'b1;
      val_o <= 1'b1;
      tx_wait <= 1'b0;
      tx_lock <= 1'b1;

    // Locked in starting TX
    end else if (!tx_busy && tx_lock) begin

      tx_start <= 1'b1;
      val_o <= 1'b0;

    // TX has started transmission
    end else if (tx_busy && tx_lock) begin

      tx_start <= 1'b0;
      val_o <= 1'b0;
      tx_lock <= 1'b0;

    // RX Buffer Full
    end else if (rx_full && !rx_wait) begin

      data_o[7:0] <= rx_data;
      rx_wait <= 1'b1;
      int_o <= 1'b1;

    // RX Buffer Full, waiting for processor to ack
    end else if (!ce_i && rx_full && rx_wait) begin

      int_o <= 1'b1;

    // Idle
    end else begin

      tx_start <= 1'b0;
      val_o <= 1'b0;

    end

  end

  uart_tx     #(.REF_CLK    (REF_CLK),
                .BAUDRATE   (BAUDRATE))
          TX_0 (.clk_i      (clk_i),
                .tx_start   (tx_start),
                .tx_data    (tx_temp_buf),
                .txd_o      (txd_o),
                .tx_busy    (tx_busy));


  uart_rx     #(.REF_CLK    (REF_CLK),
                .BAUDRATE   (BAUDRATE),
                .OVERSAMP   (8))
          RX_0 (.clk_i      (clk_i),
                .rxd_i      (rxd_i),
                .rx_clear   (rx_clear),
                .rx_data    (rx_data),
                .rx_full    (rx_full));

endmodule


// Once tx_start is pulled high, tx_data is read and transmitted onto txd_out
module uart_tx (

	input clk_i,
	input tx_start,
	input [7:0] tx_data,

	output reg txd_o,
	output reg tx_busy

);

  parameter REF_CLK  = 10000000;	// 10MHz
  parameter BAUDRATE = 9600;

  wire tx_clk;

  reg [3:0] tx_ptr = 0;

  // Transmit Shift register
  reg [7:0] tx_buf = "C";

  // Transmitting
  reg tx_on;


  uart_pulse_gen    #(.REF_CLK  (REF_CLK),
                      .BAUDRATE (BAUDRATE))
          UART_TX_GEN(.clk_i    (clk_i),
                      .clr_i    (1'b0),
                      .pulse_o  (tx_clk));

  initial begin
    
    txd_o <= 1'b1;
    tx_busy <= 1'b0;
    tx_on <= 1'b0;

  end

  always @(posedge clk_i) begin
    
    if (tx_start) begin
      
      tx_busy <= 1'b1;

    end else if (!tx_on && (tx_ptr == 4'b1100) && tx_clk) begin

      tx_busy <= 1'b0;

    end

  end

  // TX Controller
  always @(posedge clk_i) begin

    if (tx_clk) begin
      if (tx_ptr == 4'b0000 && !tx_on) begin
        
        if (tx_busy) begin

          tx_ptr <= tx_ptr + 4'b0001;

          tx_buf <= tx_data;

          tx_on <= 1'b1;

        end

      end else if (tx_ptr == 4'b1011) begin
        
        tx_ptr <= tx_ptr + 4'b0001;

        tx_on <= 1'b0;

      end else begin
        
        tx_ptr <= tx_ptr + 4'b0001;

      end

      case (tx_ptr)
        4'b0000: txd_o <= 1'b1;     // Redundant (Stop) bit
        4'b0001: txd_o <= 1'b0;     // Start bit
        4'b0010: txd_o <= tx_buf[0];
        4'b0011: txd_o <= tx_buf[1];
        4'b0100: txd_o <= tx_buf[2];
        4'b0101: txd_o <= tx_buf[3];
        4'b0110: txd_o <= tx_buf[4];
        4'b0111: txd_o <= tx_buf[5];
        4'b1000: txd_o <= tx_buf[6];
        4'b1001: txd_o <= tx_buf[7];
        4'b1010: txd_o <= 1'b1;     // Stop bit
        default: txd_o <= 1'b1;
      endcase

    end

  end

endmodule


module uart_rx (

	input clk_i,
	input rxd_i,
	input rx_clear,

	output reg [7:0] rx_data,
	output reg rx_full

);

  parameter REF_CLK  = 10000000;	// 10MHz
  parameter BAUDRATE = 9600;
  parameter OVERSAMP = 8;

  wire rx_clk;

  reg rx_clk_clr;

  reg [1:0] rx_edge;

  // Count of rx samples (holds 8 bit sample count)
  reg [3:0] rx_sample;

  // Count of rx samples taken
  reg [3:0] rx_count;

  // Count of rx bits recieved
  reg [3:0] rx_total;

  reg recieving;
  reg recieved;

  initial begin
    rx_data = "R";
    rx_full <= 1'b0;
    rx_clk_clr <= 1'b0;
    rx_sample <= 4'b0;
    rx_count <= 4'b0;
    recieved <= 1'b0;
  end


  uart_pulse_gen    #(.REF_CLK  (REF_CLK),
                      .BAUDRATE (BAUDRATE),
                      .OVERSAMP (OVERSAMP))
          UART_RX_GEN(.clk_i    (clk_i),
                      .clr_i    (rx_clk_clr),
                      .pulse_o  (rx_clk));

  // Edge detector
  always @(posedge clk_i) begin
    
    rx_edge = {rx_edge[0], rxd_i};

  end

  // State transitions
  always @(posedge clk_i) begin
  
    rx_clk_clr <= 1'b0;
    
    // Start bit edge detect
    if (rx_edge == 2'b10 && !recieving && !recieved) begin
      
      recieving <= 1'b1;
      rx_clk_clr <= 1'b1;

    end else if (recieving && !recieved) begin
      
      recieving <= 1'b1;

    end else if (recieving && recieved) begin
      
      recieving <= 1'b0;
      rx_full <= 1'b1;

    end else if (rx_clear) begin

      rx_full <= 1'b0;

    end

  end

  // Shift register/Sampling unit
  always @(posedge clk_i) begin
  
    recieved <= 1'b0;
    
    if (recieving && rx_clk) begin
      
      // Shifts in 9 bits (including the start bit)
      if (rx_total == 4'b1001) begin

        recieved <= 1'b1;
      
      // Samples each bit 8x
      end else if (rx_count == 4'b0111) begin

        // If sample was 0
        if ((rx_sample+rxd_i) < 4'b0100) begin
          
          rx_data <= {1'b0, rx_data[7:1]};

        // If sample was 1
        end else begin

          rx_data <= {1'b1, rx_data[7:1]};

        end
        rx_count <= 4'b0000;//rx_count + 1;
        rx_sample <= 4'b0000;
        rx_total <= rx_total + 1;

      // Perform sampling
      end else begin

        rx_sample <= rx_sample + rxd_i;
        rx_count <= rx_count + 1;

      end

    // Does nothing while rx_clk is not high
    end else if (recieving && !rx_clk) begin

    // Resets sampling
    end else begin
      
      rx_count <= 4'b0;
      rx_sample <= 4'b0;
      //rx_data <= 8'b0;
      rx_total <= 4'b0;

    end

  end

endmodule

// Clock generator that outputs baud rate * oversampling
module uart_clock_gen (

	input clk_i,
  input clr_i,

	output reg clk_o

);

  parameter REF_CLK  = 10000000;
  parameter BAUDRATE = 9600;
  parameter OVERSAMP = 1;

  reg [15:0] count;

  initial begin
    count <= 0;
    clk_o <= 0;
  end

  // Generate Transmit clock
  always @(posedge clk_i) begin
    
    if (clr_i) begin

      clk_o <= 0;
      count <= 16'b0;

    end else if (count == ((REF_CLK)/(2*BAUDRATE*OVERSAMP)) - 1) begin

      clk_o <= !clk_o;

      count <= 16'b0;

    end else begin

      count <= count + 1;

    end
    

  end

endmodule

// Pulse generator that outputs baud rate * oversampling
module uart_pulse_gen (

	input clk_i,
  input clr_i,

	output reg pulse_o

);

  parameter REF_CLK  = 10000000;
  parameter BAUDRATE = 9600;
  parameter OVERSAMP = 1;

  reg [15:0] count;

  initial
    count <= 0;

  // Generate Transmit clock
  always @(posedge clk_i) begin
    
    pulse_o <= 1'b0;

    if (clr_i) begin

      count <= 16'b0;

    end else if (count == ((REF_CLK)/(BAUDRATE*OVERSAMP)) - 1) begin

      pulse_o <= 1'b1;

      count <= 16'b0;

    end else begin

      count <= count + 1;

    end
    

  end

endmodule
