// 3 bit 2-1 Mux
module mux2_1x3 (a, b, sel, out);
    
    input [2:0] a, b;
    input sel;

    output reg [2:0] out;

    always @(*) begin
        
        if (sel == 0)
            out <= a;
        else //if (sel == 1)
            out <= b;

    end

endmodule
