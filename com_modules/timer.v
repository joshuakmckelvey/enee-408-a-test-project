// 64 bit timer
module timer (

  input [31:0] data_i,
  input clk_i, rd_wr_i, cur_cmp_i, low_high_i, ce_i,
  
  output reg [31:0] data_o,
  output reg val_o,
  output wire int_o

);

  // Timer that counts up every cycle
  reg [63:0] cur_time;

  // Triggers timer interrupt when cur_time >= cmp_time
  reg [63:0] cmp_time;

  reg [31:0] dout;

  reg [31:0] din;

  reg cur_wr, cmp_wr;

  initial begin
    cur_time = 64'h0;
    cmp_time = 64'hFFFFFFFFFFFFFFFF; // Set to large enough to not trigger
  end

  // Output is valid when accessed
  always @(posedge clk_i) begin
    
    if (ce_i)
      val_o <= 1'b1;
    else
      val_o <= 1'b0;

  end

  // Reorder inputs and outputs to little endian
  always @(*) begin
    
    data_o = {dout[7:0], dout[15:8], dout[23:16], dout[31:24]};
    din    = {data_i[7:0], data_i[15:8], data_i[23:16], data_i[31:24]};

  end

  // Output control
  always @(*) begin

      if (!cur_cmp_i)
        if (!rd_wr_i)
          if (low_high_i)
            dout <= cmp_time[63:32];
          else
            dout <= cmp_time[31:0];
        else 
          dout <= cmp_time[31:0]; // Output doesn't matter
      else
        if (!rd_wr_i)
          if (low_high_i)
            dout <= cur_time[63:32];
          else
            dout <= cur_time[31:0];
        else 
          dout <= cur_time[31:0]; // Output doesn't matter

  end

  // Write control
  always @(*) begin

    if (ce_i && !cur_cmp_i && rd_wr_i)
      cmp_wr = 1'b1;
    else
      cmp_wr = 1'b0;


    if (ce_i && cur_cmp_i && rd_wr_i)
      cur_wr = 1'b1;
    else
      cur_wr = 1'b0;
  
  end

  // State transitions
  always @(posedge clk_i) begin

    if (cur_wr)
      if (low_high_i)
        cur_time[63:32] <= din;
      else
        cur_time[31:0] <= din;
    else
      cur_time <= cur_time+1;

    if (cmp_wr)
      if (low_high_i)
        cmp_time[63:32] <= din;
      else
        cmp_time[31:0] <= din;

  end

  assign int_o = (cur_time >= cmp_time);

endmodule
