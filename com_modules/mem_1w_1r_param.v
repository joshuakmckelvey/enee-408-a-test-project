// Parameterised Simple Dual Port RAM
// Reads old value
module mem_1w_1r_param (

  input clock,
  input [(DATA_WIDTH-1):0] data,
  input [(ADDR_WIDTH-1):0] rdaddress, wraddress,
  input wren,
  output reg [(DATA_WIDTH-1):0] q

);

  parameter FILE = "";
  parameter ADDR_WIDTH = 10;
  parameter DATA_WIDTH = 8;


  // Declare the RAM variable
  reg [DATA_WIDTH-1:0] ram[2**ADDR_WIDTH-1:0];

  integer i;

  initial begin

    if (FILE != "") begin

      $readmemh(FILE, ram);
      
    end else begin
      
      for (i = 0; i < 2**ADDR_WIDTH-1; i = i + 1)
        ram[i] <= 0;

    end

  end

  // Read Port
  always @ (posedge clock) begin
    q <= ram[rdaddress];
  end

  // Write port
  always @ (posedge clock) begin
    if (wren) begin
      ram[wraddress] <= data;
    end
  end

endmodule
