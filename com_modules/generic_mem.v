// Main memory with controller
module generic_mem (

  input clk_i, en_i,
  input [31:0] data_i,
  input [31:0] addr_i,

  input dir_i,

  output [31:0] data_o,

  output reg val_o

);

  parameter FILE = "";
  parameter ADDR_WIDTH = 14;

  reg [(ADDR_WIDTH-1):2] read_addr;

  initial begin
    if (FILE != "") begin
      $readmemh({FILE, "_0.dat"}, mem0);
      $readmemh({FILE, "_1.dat"}, mem1);
      $readmemh({FILE, "_2.dat"}, mem2);
      $readmemh({FILE, "_3.dat"}, mem3);
    end

  end

  // Main Memory
  reg [7:0] mem0 [0:((1<<ADDR_WIDTH)-1)];
  reg [7:0] mem1 [0:((1<<ADDR_WIDTH)-1)];
  reg [7:0] mem2 [0:((1<<ADDR_WIDTH)-1)];
  reg [7:0] mem3 [0:((1<<ADDR_WIDTH)-1)];

  // Memory address register
  reg [31:0] mem_addr;

  // Control lines
  reg mem_addr_ld;
  reg mem_addr_inc;
  reg internal_we;

  // State machine regs
  reg [3:0] state;
  reg [3:0] next_state;

  // State machine next state
  always @(*) begin
    
    if (state == 4'b0000) begin

      if (en_i && !dir_i) begin
        
        next_state = 4'b0001;

      end else if (en_i && dir_i) begin
        
        next_state = 4'b1001;

      end else begin
        
        next_state = 4'b0000;

      end

    end else if (state == 4'b0100) begin

        next_state = 4'b0000;

    end else if (state == 4'b1110) begin

        next_state = 4'b0000;

    end else begin

      next_state = state + 1;

    end

  end

  // State machine transition
  always @(posedge clk_i) begin
    
    state <= next_state;

  end

  // Memory controller initial state  
  initial begin
    
    state <= 4'b0000;

  end

  // Control lines
  always @(*) begin

    // Defaults
    mem_addr_ld   = 1'b0;
    mem_addr_inc  = 1'b0;
    val_o         = 1'b0;
    internal_we   = 1'b0;

    case (state)
      4'b0000: begin
        mem_addr_ld   = 1'b1;
        mem_addr_inc  = 1'b0;
        val_o         = 1'b0;
      end
      4'b0001: begin
        mem_addr_ld   = 1'b0;
        mem_addr_inc  = 1'b1;
        val_o         = 1'b0;
      end
      4'b0010: begin
        mem_addr_ld   = 1'b0;
        mem_addr_inc  = 1'b1;
        val_o         = 1'b1;
      end
      4'b0011: begin
        mem_addr_ld   = 1'b0;
        mem_addr_inc  = 1'b1;
        val_o         = 1'b1;
      end
      4'b0100: begin
        mem_addr_ld   = 1'b0;
        mem_addr_inc  = 1'b0;
        val_o         = 1'b1;
      end
      // Store
      4'b1001: begin
        mem_addr_ld   = 1'b0;
        mem_addr_inc  = 1'b0;
        internal_we   = 1'b0;
        val_o         = 1'b1;
      end
      4'b1010: begin
        mem_addr_ld   = 1'b0;
        mem_addr_inc  = 1'b0;
        internal_we   = 1'b0;
        val_o         = 1'b0;
      end
      4'b1011: begin
        mem_addr_ld   = 1'b0;
        mem_addr_inc  = 1'b1;
        internal_we   = 1'b1;
        val_o         = 1'b0;
      end
      4'b1100: begin
        mem_addr_ld   = 1'b0;
        mem_addr_inc  = 1'b1;
        internal_we   = 1'b1;
        val_o         = 1'b0;
      end
      4'b1101: begin
        mem_addr_ld   = 1'b0;
        mem_addr_inc  = 1'b1;
        internal_we   = 1'b1;
        val_o         = 1'b0;
      end
      4'b1110: begin
        mem_addr_ld   = 1'b0;
        mem_addr_inc  = 1'b0;
        internal_we   = 1'b1;
        val_o         = 1'b0;
      end
    endcase

  end

  // Memory Address register
  always @(posedge clk_i) begin
    
    if (mem_addr_ld) begin
      
      mem_addr <= {addr_i[31:4], 4'b00}; // line aligned access

    end else if (mem_addr_inc) begin

      mem_addr <= mem_addr + 4;

    end

  end

  always @(posedge clk_i) begin
    
    if (internal_we)
      mem0[mem_addr[(ADDR_WIDTH-1):2]] <= data_i[31:24];

    if (internal_we)
      mem1[mem_addr[(ADDR_WIDTH-1):2]] <= data_i[23:16];

    if (internal_we)
      mem2[mem_addr[(ADDR_WIDTH-1):2]] <= data_i[15:8];

    if (internal_we)
      mem3[mem_addr[(ADDR_WIDTH-1):2]] <= data_i[7:0];


    read_addr <= mem_addr[(ADDR_WIDTH-1):2];

  end

  // Outputs currently ignore valid
  assign data_o[31:24] = mem0[read_addr];
  assign data_o[23:16] = mem1[read_addr];
  assign data_o[15:8]  = mem2[read_addr];
  assign data_o[7:0]   = mem3[read_addr];

endmodule
