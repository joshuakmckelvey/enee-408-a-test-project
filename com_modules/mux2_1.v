// 2-1 Mux
module mux2_1 (a, b, sel, out);
    
    input a, b;
    input sel;

    output reg out;

    always @(*) begin
        
        if (sel == 0)
            out <= a;
        else //if (sel == 1)
            out <= b;

    end

endmodule
