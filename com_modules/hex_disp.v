// Hex display decoder
module hex_disp(in, ca);

    input [3:0] in;
    output reg [6:0] ca;
        
    always @ (*)
      case(in)
        4'b0000: //hex 0
        ca = 7'b1000000;
        4'b0001: //hex 1
        ca = 7'b1111001;
        4'b0010: //hex 2
        ca = 7'b0100100;
        4'b0011: //hex 3
        ca = 7'b0110000;
        4'b0100: //hex 4
        ca = 7'b0011001;
        4'b0101: //hex 5
        ca = 7'b0010010;
        4'b0110: //hex 6
        ca = 7'b0000010;
        4'b0111: //hex 7
        ca = 7'b1111000;
        4'b1000: //hex 8
        ca = 7'b0000000;
        4'b1001: //hex 9
        ca = 7'b0010000;
        4'b1010: //hex A
        ca = 7'b0001000;
        4'b1011: //hex B
        ca = 7'b0000011;
        4'b1100: //hex C
        ca = 7'b1000110;
        4'b1101: //hex D
        ca = 7'b0100001;
        4'b1110: //hex E
        ca = 7'b0000110;
        4'b1111: //hex F
        ca = 7'b0001110;
      endcase

endmodule
