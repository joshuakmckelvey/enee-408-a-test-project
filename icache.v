// Instruction cache
// Port 0 is read only, port 1 is write only
module icache (clk_i,  addr_0_i, addr_1_i, data_1_i, wr_en_1_i,
               stall_0_i, data_0_o);

  parameter FILE = "";
  parameter ADDR_WIDTH = 12;

  input clk_i;
  input [31:0] addr_0_i, addr_1_i;
  input [31:0] data_1_i;
  input wr_en_1_i;

  input stall_0_i;              // Stalls Port 0 Address

  output [31:0] data_0_o;

  reg [(ADDR_WIDTH-1):2] read_addr_0, read_addr_1;

  initial begin
    if (FILE != "") begin
      $readmemh({FILE, "_0.dat"}, mem0);
      $readmemh({FILE, "_1.dat"}, mem1);
      $readmemh({FILE, "_2.dat"}, mem2);
      $readmemh({FILE, "_3.dat"}, mem3);
    end

  end

  // Main Memory
  reg [7:0] mem0 [0:((1<<ADDR_WIDTH)-1)];
  reg [7:0] mem1 [0:((1<<ADDR_WIDTH)-1)];
  reg [7:0] mem2 [0:((1<<ADDR_WIDTH)-1)];
  reg [7:0] mem3 [0:((1<<ADDR_WIDTH)-1)];

  always @(posedge clk_i) begin
    
    if (wr_en_1_i)
      mem0[addr_1_i[(ADDR_WIDTH-1):2]] <= data_1_i[31:24];

    if (wr_en_1_i)
      mem1[addr_1_i[(ADDR_WIDTH-1):2]] <= data_1_i[23:16];

    if (wr_en_1_i)
      mem2[addr_1_i[(ADDR_WIDTH-1):2]] <= data_1_i[15:8];

    if (wr_en_1_i)
      mem3[addr_1_i[(ADDR_WIDTH-1):2]] <= data_1_i[7:0];


    if (!stall_0_i) begin
      read_addr_0 <= addr_0_i[(ADDR_WIDTH-1):2];
    end

  end

/*
  // Output port 0 (No Data Forwarding)
  assign data_0_o[31:24] = mem0[read_addr_0];
  assign data_0_o[23:16] = mem1[read_addr_0];
  assign data_0_o[15:8]  = mem2[read_addr_0];
  assign data_0_o[7:0]   = mem3[read_addr_0];
*/
  assign data_0_o[31:24] = mem3[read_addr_0];
  assign data_0_o[23:16] = mem2[read_addr_0];
  assign data_0_o[15:8]  = mem1[read_addr_0];
  assign data_0_o[7:0]   = mem0[read_addr_0];

  initial begin
    
    read_addr_0 = 0;

  end

endmodule
