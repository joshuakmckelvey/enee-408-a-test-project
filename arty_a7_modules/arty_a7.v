// Arty A7 interface for kirv system
module arty_a7 (

  input sys_clk,
  //input [1:0] KEY,
  output [3:0] led,
  //input [3:0] SW,

  // PMOD Ports A & B (Used for VGA)
  output [7:0] ja,
  output [7:0] jb,

  // UART
  input uart_txd_in,
  output uart_rxd_out

);

  wire single_step;

  wire clk_pll;

  wire [31:0] rled_reg;

  wire [7:0] vga_r_8b, vga_b_8b;
  wire [7:0] vga_g_8b;


  kirv_soc        #(.CLK_RATE   (25125000))
           SYSTEM  (.clk_i      (clk_pll),
                    .switch_i   (4'b0),
                    .key_i      (4'b1111),
                    .uart_rxd   (uart_txd_in),  // Note: Perspective is reversed
                    .uart_txd   (uart_rxd_out),
                    .hex_disp   (),
                    .rled_reg_o (rled_reg),
                    .vga_r      (vga_r_8b),
                    .vga_g      (vga_g_8b),
                    .vga_b      (vga_b_8b),
                    .vga_hs     (vga_hs),
                    .vga_vs     (vga_vs),
                    .vga_blank  (),
                    .vga_sync   (),
                    .lcd_data_o (),
                    .lcd_ctrl_o ());

  // Clock PLL (100MHz to 25.125MHz)
  pll_100m_25p125m PLL (.clk_in1   (sys_clk),
                    .clk_out1  (clk_pll));


  // PMOD VGA Module
  assign ja[3:0] = vga_r_8b[7:4];
  assign jb[3:0] = vga_g_8b[7:4];
  assign ja[7:4] = vga_b_8b[7:4];
  assign jb[4] = vga_hs;
  assign jb[5] = vga_vs;

  assign led[0]   = 1'b1;
  assign led[3:1] = 7'b0;
	
endmodule
