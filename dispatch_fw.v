// Dispatch and Forwarding Control

`define PROD_RF 2'b00
`define PROD_NA 2'b00
`define PROD_EX 2'b01
`define PROD_MA 2'b10

`define FW_RS1_SEL_RF   2'b00
`define FW_RS1_SEL_EX   2'b01
`define FW_RS1_SEL_MA   2'b10

`define FW_RS2_SEL_RF   2'b00
`define FW_RS2_SEL_EX   2'b01
`define FW_RS2_SEL_MA   2'b10

module dispatch_fw (rs1_fw, rs2_fw, rs1_if_rf, rs2_if_rf, rd_rf_ex, rd_ex_ma,
                    prod_rf_ex, prod_ex_ma, val_if_rf, val_rf_ex, val_ex_ma,
                    fw_rs1_mux_sel, fw_rs2_mux_sel, dp_fw_stall_req);

  input rs1_fw, rs2_fw;                 // Destination operand forward enable
  input [4:0] rs1_if_rf, rs2_if_rf;     // Destination operands
  input [4:0] rd_rf_ex, rd_ex_ma;       // Source operands
  input [1:0] prod_rf_ex, prod_ex_ma;   // Source operand production stage
                                        // (prod_ex_ma is currently unused)
  input val_if_rf, val_rf_ex, val_ex_ma;// Stage valid

  output reg [1:0] fw_rs1_mux_sel, fw_rs2_mux_sel;  // Forwarding mux select
  output dp_fw_stall_req;                           // Stall request

  reg rs1_stall_req;  // Stall requests for each forwarding path
  reg rs2_stall_req;

  always @(*) begin
    
    // RS1 Mux Forwarding Logic
    if (val_if_rf && rs1_fw && (rs1_if_rf != 5'b00000)) begin
      if ((rs1_if_rf == rd_rf_ex) && val_rf_ex) begin // EX Forward
        if (prod_rf_ex == `PROD_EX) begin
          fw_rs1_mux_sel = `FW_RS1_SEL_EX;
          rs1_stall_req = 1'b0;
        end else begin
          fw_rs1_mux_sel = `FW_RS1_SEL_EX;  // X
          rs1_stall_req = 1'b1;
        end
      end else if ((rs1_if_rf == rd_ex_ma) && val_ex_ma) begin // MA Forward
        fw_rs1_mux_sel = `FW_RS1_SEL_MA;
        rs1_stall_req = 1'b0;
      end else begin
        fw_rs1_mux_sel = `FW_RS1_SEL_RF;
        rs1_stall_req = 1'b0;
      end
    end else begin
      fw_rs1_mux_sel = `FW_RS1_SEL_RF;
      rs1_stall_req = 1'b0;
    end

    // RS2 Mux Forwarding Logic
    if (val_if_rf && rs2_fw && (rs2_if_rf != 5'b00000)) begin
      if ((rs2_if_rf == rd_rf_ex) && val_rf_ex) begin // EX Forward
        if (prod_rf_ex == `PROD_EX) begin
          fw_rs2_mux_sel = `FW_RS2_SEL_EX;
          rs2_stall_req = 1'b0;
        end else begin
          fw_rs2_mux_sel = `FW_RS2_SEL_EX;  // X
          rs2_stall_req = 1'b1;
        end
      end else if ((rs2_if_rf == rd_ex_ma) && val_ex_ma) begin // MA Forward
        fw_rs2_mux_sel = `FW_RS2_SEL_MA;
        rs2_stall_req = 1'b0;
      end else begin
        fw_rs2_mux_sel = `FW_RS2_SEL_RF;
        rs2_stall_req = 1'b0;
      end
    end else begin
      fw_rs2_mux_sel = `FW_RS2_SEL_RF;
      rs2_stall_req = 1'b0;
    end

  end

  assign dp_fw_stall_req = rs1_stall_req || rs2_stall_req;

endmodule
