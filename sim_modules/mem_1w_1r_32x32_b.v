module mem_1w_1r_32x32 (
	clock,
	data,
	rd_addressstall,
	rdaddress,
	wraddress,
	wren,
	q);

	input	  clock;
	input	[31:0]  data;
	input	  rd_addressstall;
	input	[4:0]  rdaddress;
	input	[4:0]  wraddress;
	input	  wren;
	output	[31:0]  q;

  reg [4:0] rdaddress_buf;
  integer i;

  initial begin

    rdaddress_buf <= 32'b0;

    for (i = 0; i < 32; i = i + 1)
      mem[i] <= 32'b0;

  end

  // Memory
  reg [31:0] mem [0:31];


  always @(posedge clock) begin
    
    if (wren)
      mem[wraddress] <= data;

    if (!rd_addressstall)
      rdaddress_buf <= rdaddress;

  end

  assign q = mem[rdaddress_buf];

endmodule
