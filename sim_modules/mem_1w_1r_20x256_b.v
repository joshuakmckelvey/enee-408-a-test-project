module mem_1w_1r_20x256 (
	data,
	rdaddress,
	wraddress,
	clock,
  rd_addressstall,
	wren,
	q);

	input	[19:0]  data;
	input	[7:0]  rdaddress;
	input	  clock;
	input	[7:0]  wraddress;
  input rd_addressstall;
	input	  wren;
	output	[19:0]  q;


  reg [7:0] rdaddress_buf;
  integer i;

  initial begin

    rdaddress_buf <= 8'b0;

    for (i = 0; i < 256; i = i + 1)
      mem[i] <= 20'b0;

  end

  reg [19:0] mem [0:255];


  always @(posedge clock) begin
    
    if (wren)
      mem[wraddress] <= data;

    if (!rd_addressstall)
      rdaddress_buf <= rdaddress;

  end

  assign q = mem[rdaddress_buf];

endmodule
