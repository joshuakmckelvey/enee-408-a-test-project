module mem_1w_1r_1x256 (
	clock,
	data,
	rd_addressstall,
	rdaddress,
	wraddress,
	wren,
	q);

	input	  clock;
	input	[0:0]  data;
	input	  rd_addressstall;
	input	[7:0]  rdaddress;
	input	[7:0]  wraddress;
	input	  wren;
	output	[0:0]  q;


  reg [7:0] rdaddress_buf;
  integer i;

  initial begin

    rdaddress_buf <= 8'b0;

    for (i = 0; i < 256; i = i + 1)
      mem[i] <= 1'b0;

  end

  reg [0:0] mem [0:255];


  always @(posedge clock) begin
    
    if (wren)
      mem[wraddress] <= data;

    if (!rd_addressstall)
      rdaddress_buf <= rdaddress;

  end

  assign q = mem[rdaddress_buf];

endmodule
