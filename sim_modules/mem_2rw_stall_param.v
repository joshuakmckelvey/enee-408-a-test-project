// Parameterised True Dual Port RAM With Stall
// Reads new value on same port, old value on different ports
module mem_2rw_stall_param (

  input clock,
  input [(DATA_WIDTH-1):0] data_a, data_b,
  input [(ADDR_WIDTH-1):0] address_a, address_b,
  input addressstall_a,
  input wren_a, wren_b,
  output [(DATA_WIDTH-1):0] q_a, q_b

);

  parameter FILE = "";
  parameter ADDR_WIDTH = 14-2;
  parameter DATA_WIDTH = 32;

  reg [(ADDR_WIDTH-1):0] address_buf_a;
  reg [(ADDR_WIDTH-1):0] address_buf_b;

  reg [DATA_WIDTH-1:0] mem [0:2**ADDR_WIDTH-1];

  integer i;

  initial begin

    address_buf_a <= 0;
    address_buf_b <= 0;

    for (i = 0; i < 2**ADDR_WIDTH-1; i = i + 1)
      mem[i] <= 0;

  end

  always @(posedge clock) begin
    
    if (wren_a)
      mem[address_a] <= data_a;

    if (wren_b)
      mem[address_b] <= data_b;

    if (!addressstall_a)
      address_buf_a <= address_a;

    address_buf_b <= address_b;

  end

  assign q_a = mem[address_buf_a];
  assign q_b = mem[address_buf_b];

endmodule
