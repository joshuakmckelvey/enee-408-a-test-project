// L2 Cache, 16KB
module l2cache (

  input clk_i,
  input [31:0] addr_0_i, addr_1_i,
  input [31:0] data_0_i,
  input [31:0] data_1_i,
  input wr_en_0_i,
  input wr_en_1_i,

  output [31:0] data_0_o, data_1_o

);

  parameter FILE = "";
  parameter ADDR_WIDTH = 14-2;


  mem_2rw_param       #(.FILE       (FILE),
                        .ADDR_WIDTH (ADDR_WIDTH),
                        .DATA_WIDTH (32)) 
                 L2MEM (.address_a  (addr_0_i[ADDR_WIDTH+1:2]),
                        .address_b  (addr_1_i[ADDR_WIDTH+1:2]),
                        .clock      (clk_i),
                        .data_a     (data_0_i),
                        .data_b     (data_1_i),
                        .wren_a     (wr_en_0_i),
                        .wren_b     (wr_en_1_i),
                        .q_a        (data_0_o),
                        .q_b        (data_1_o));
endmodule
