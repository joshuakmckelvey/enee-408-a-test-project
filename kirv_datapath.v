// Datapath specification of KIRV 5 Stage Piplined Design

// Definitions for Instruction Fields
`define INST_OP     6:2   // major opcode
`define INST_F3     14:12 // minor F3 opcode
`define INST_F7     31:25 // minor F7 opcode

`define INST_RD     11:7  // rd
`define INST_RS1    19:15 // rs1
`define INST_RS2    24:20 // rs2

`define INST_CSR    31:20 // CSR field

`define CACHE_VAL   1'b1  // Valid cache entry

// Definition of Zero for the processor
`define ZERO        32'd0

module kirv_datapath (
    
  input clk_i,

  // Interrupt inputs [external interrupt, timer interrupt]
  input [1:0] int_i,

  // Requests the processor to stall
  input ext_stall_req_i,

  // Cache refill data and address lines
  input [31:0] ext_data_i, ext_addr_i,

  // Cache refill control lines
  input ext_icache_acc_i, ext_itag_dir_i, ext_dcache_acc_i, ext_dcache_dir_i,
        ext_dcache_val_i, ext_dcache_dty_i, unc_sel_i,

  input [3:0] ext_mask_i,

  // Cache refill/evict line
  output [31:0] icache_addr_o, dcache_data_o, dcache_addr_o, unc_data_o,

  // Cache refill request lines
  output icache_miss_o, dcache_dir_o, dcache_miss_o, dcache_dty_o, unc_acc_o,
  output [19:0] dcache_tag_o,

  output [3:0] dcache_mask_o

);

  // BP-IF Pipeline Registers
  reg [31:0] pc_bp_if;

  // IF-RF Pipeline Registers
  reg [31:0] pc_if_rf;
  reg [4:0] rs1_if_rf, rs2_if_rf;
  reg [31:0] ir_if_rf;      // op is currently not separated
  reg val_if_rf;            // Current instruction is valid

  // RF-EX Pipeline Registers
  reg [31:0] pc_rf_ex;
  reg [31:0] s1_rf_ex, s2_rf_ex, imm_rf_ex, csr_res_rf_ex;
  reg [2:0] csr_dest_rf_ex; // Encoded csr file address
  reg csr_val_rf_ex;
  reg [2:0] f3_rf_ex;
  reg [1:0] f7e_rf_ex;      // Encoded F7 field
  reg [4:0] rd_rf_ex;
  reg cmp_op_rf_ex;         // Is a branch instruction
  reg jmp_op_rf_ex;         // Is a jump instruction
  reg ld_op_rf_ex;          // Is a load instruction
  reg st_op_rf_ex;          // Is a store instruction
  reg csr_op_rf_ex;         // Is a csr instruction
  reg inp_sel1_rf_ex;       // Selects ALU input (pc/register)
  reg inp_sel2_rf_ex;       // Selects ALU input (register/imm)
  reg [1:0] ex_sel_rf_ex;   // Selects EX state result (alu/imm/pc+4/csr)
  reg [2:0] ecause_rf_ex;   // Encodes call/ret/invalid instruction types
  reg [1:0] prod_rf_ex;     // Encodes which stage the result is produced in
  reg val_rf_ex;

  // EX-MA Pipeline Registers
  reg [31:0] pc_ex_ma;
  reg [31:0] alu_ex_ma;
  reg [3:0] mask_ex_ma;     // Data cache byte enable
  reg [2:0] f3_ex_ma;       // Data cache RD port control bits
  reg [31:0] cdata_ex_ma;   // Output of rs1 for store instruction
  reg st_op_ex_ma;          // Is a store instruction
  reg [4:0] rd_ex_ma;
  reg [2:0] csr_dest_ex_ma;
  reg csr_op_ex_ma;
  reg [31:0] csr_res_ex_ma; // Result of CSR Inst
  reg [2:0] ecause_ex_ma;
  reg [1:0] prod_ex_ma;
  reg val_ex_ma;
  reg ls_op_ex_ma;
  reg [1:0] int_ex_ma;

  // MA-WB Pipeline Registers
  reg [31:0] pc_ma_wb;
  reg [31:0] res_ma_wb;     // Result to be stored in RF
  reg [4:0] rd_ma_wb;
  reg [31:0] caddr_ma_wb;   // Cache store address
  reg [3:0] mask_ma_wb;     // WB data cache port byte enable
  reg [31:0] cdata_ma_wb;   // WB data cache port data reg
  reg cdir_ma_wb;           // WB data cache port is either storing or loading
                            // (Possibly both in the future?-faster evict?)
  reg cval_ma_wb;
  reg cdty_ma_wb;
  reg [31:0] csr_res_ma_wb; // Result to be stored in the CSR RF
  reg [2:0] csr_dest_ma_wb;
  reg csr_op_ma_wb;
  reg [2:0] ecause_ma_wb;   // Encodes final exception cause
  reg [1:0] int_ma_wb;
  
  
  // BP Stage Wires

  wire [31:0] pc4_out, pc_mux_out;
  wire [1:0] npc_sel_out;

  // IF Stage Wires

  wire [31:0] icache_data_out;
  wire [19:0] icache_tag_out;
  wire icache_val_out;
  wire if_stall_req;

  // RF Stage Wires

  wire [31:0] rs1_port_out, rs2_port_out;
  wire [31:0] s1_out, s2_out;
  wire [31:0] imm_out;
  wire [2:0] csr_dest_out;
  wire csr_val_out;
  wire [31:0] csr_res_out;
  wire cmp_op_out, jmp_op_out, ld_op_out, st_op_out, csr_op_out;
  wire inp_sel1_out, inp_sel2_out;
  wire [1:0] ex_sel_out;
  wire [2:0] ecause_out;
  wire [1:0] prod_out;
  wire rs1_fw_out, rs2_fw_out;
  wire [2:0] dec_f3_out;
  wire [1:0] dec_f7e_out; // Encoded F7 field
  wire [4:0] dec_rd_out;

  wire [1:0] priv;    // Current processor privilege
  wire [31:0] mtvec;
  wire [31:0] mepc;
  wire [31:0] mie;
  wire [31:0] mstatus;

  // EX Stage Wires

  wire [31:0] inp1_mux_out, inp2_mux_out;
  wire [31:0] alu_out;
  wire [31:0] alu_mux_out;
  wire [31:0] pc4_rf_ex_out;
  wire br_tak;
  wire [3:0] dag_mask_out;
  wire [31:0] dag_data_out;
  wire dag_st_op_out;
  wire [31:0] csr_lu_out;

  // MA Stage Wires

  wire [31:0] dcache_data_out;
  wire [31:0] data_ld_ctrl_out;
  wire [31:0] ld_mux_out;
  wire [31:0] res_mux_out;
  wire [4:0] rd_com_mux_out;
  wire [31:0] caddr_mux_out;
  wire [3:0] mask_com_mux_out;
  wire [3:0] mask_ext_mux_out;
  wire [31:0] cdata_mux_out;
  wire cdir_mux_out;
  wire cval_mux_out;
  wire cdty_mux_out;
  wire csr_op_com_mux_out;
  wire [2:0] exc_arb_out;
  wire etak;
  wire [2:0] ecause_com_mux_out;
  wire dcache_val_out;
  wire pma_val_out;
  wire ma_stall_req;
  wire commit;
  wire dtag_line_val;

  // WB Stage Wires

  wire exc_tak; // Exception or Interrupt taken
  wire ret_tak; // Return Instruction taken

  // Global Control Logic Wires

  wire [1:0] fw_rs1_mux_sel, fw_rs2_mux_sel;
  wire dp_fw_stall_req;

  wire stall_bp_if;
  wire stall_if_rf;
  wire stall_rf_ex;
  wire stall_ex_ma;
  wire val_if_rf_in;
  wire val_rf_ex_in;
  wire val_ex_ma_in;
  wire val_ma_wb;   // Does not pass through pipeline register

  reg we_disabled;  // Unoptimizeable 1'b0 constant kludge

  // BP Stage Modules

  // Next PC Mux
  mux4_1x32 PC_MUX    (.a     (pc4_out),
                       .b     ({alu_out[31:1], 1'b0}), // LSB of CTI Not used
                       .c     (mtvec),
                       .d     (mepc),
                       .sel   (npc_sel_out),
                       .out   (pc_mux_out));
  
  // Address Incrementor (by 4)
  inc4 INC4           (.in    (pc_bp_if),
                       .out   (pc4_out));

  // Next PC selector
  npc_sel NPC_SEL     (.branch(br_tak),
                       .exc   (exc_tak),
                       .ret   (ret_tak),
                       .sel   (npc_sel_out));


  // IF Stage Modules
  
  // iCache Data
  icache       #(.FILE      ("text"))
         ICACHE (.clk_i     (clk_i),
                 .addr_0_i  (pc_mux_out),     // Equivalent to pc_bp_if reg
                 .addr_1_i  (ext_addr_i),
                 .data_1_i  (ext_data_i),
                 .wr_en_1_i (ext_icache_acc_i),
                 .stall_0_i (stall_bp_if),
                 .data_0_o  (icache_data_out));

  // iCache Tag (Note: Tags can not currently be invalidated)
  tag_cache    #(.VFILE     ("l1i_val"),
                 .TFILE     ("l1i_tag"))
          ITAG  (.clk_i     (clk_i),
                 .rd_addr_i (pc_mux_out),
                 .wr_addr_i (pc_bp_if),
                 .tag_i     (pc_bp_if[31:12]), // Does this need to be pc_bp_if?
                 .val_i     (`CACHE_VAL),
                 .wr_en_i   (ext_itag_dir_i),
                 .rd_stall_i(stall_bp_if),
                 .tag_o     (icache_tag_out),
                 .val_o     (icache_val_out));
                  

  // IF Stall request logic
  assign if_stall_req = !((pc_bp_if[31:12] == icache_tag_out) && icache_val_out);
  
  // Assign external wires
  assign icache_addr_o = pc_bp_if;
  

  // RF Stage Modules

  // Register file
  rf32x32_1w_2r RF    (.clk       (clk_i),
                       .wr_port   (res_mux_out),// Equivalent to res_ma_wb reg
                       .wr_en     (1'b1),  // Could change to be from commit bit
                       .wr_sel    (rd_com_mux_out),// Equivalent to rd_ma_wb reg
                       .rd_stall  (stall_if_rf),
                       .rd_a_sel  (icache_data_out[`INST_RS1]), // Equivalent to rs1_if_rf
                       .rd_b_sel  (icache_data_out[`INST_RS2]), // Equivalent to rs2_if_rf
                       .rd_a_port (rs1_port_out),
                       .rd_b_port (rs2_port_out));

  // RS1 Port Mux
  mux3_1x32 RS1_MUX   (.a     (rs1_port_out),
                       .b     (alu_mux_out),
                       .c     (res_mux_out),
                       .sel   (fw_rs1_mux_sel),
                       .out   (s1_out));
  
  // RS2 Port Mux
  mux3_1x32 RS2_MUX   (.a     (rs2_port_out),
                       .b     (alu_mux_out),
                       .c     (res_mux_out),
                       .sel   (fw_rs2_mux_sel),
                       .out   (s2_out));

  // Immediate decoder
  imm_decode IMM_DEC  (.in    (ir_if_rf),
                       .sel   (ir_if_rf[`INST_OP]),
                       .out   (imm_out));
  
  // CSR decoder
  csr_dec CSR_DEC     (.inst        (ir_if_rf),
                       .csr_dest    (csr_dest_out),
                       .csr_val     (csr_val_out));

  // CSR File
  csr_file CSR_FILE   (.clk_i       (clk_i),
                       .csr_addr_1_i(csr_dest_out),
                       .csr_addr_2_i(csr_dest_ma_wb),
                       .csr_data_2_i(csr_res_ma_wb),
                       .wr_en_2_i   (csr_op_ma_wb),
                       .ecause_i    (ecause_ma_wb),
                       .exc_tak_i   (exc_tak),
                       .ret_tak_i   (ret_tak),
                       .nmepc_i     (pc_ma_wb),
                       .int_i       (int_ma_wb),
                       .priv_o      (priv),
                       .mtvec_o     (mtvec),
                       .mepc_o      (mepc),
                       .mie_o       (mie),
                       .mstatus_o   (mstatus),
                       .csr_data_1_o(csr_res_out));

  // Function decoder is currently just wire connections

  // Instruction Decoder
  inst_dec INST_DEC   (.inst        (ir_if_rf),
                       .priv        (priv),
                       .csr_val     (csr_val_out),
                       .cmp_op      (cmp_op_out),
                       .jmp_op      (jmp_op_out),
                       .ld_op       (ld_op_out),
                       .st_op       (st_op_out),
                       .csr_op      (csr_op_out),
                       .inp_sel1    (inp_sel1_out),
                       .inp_sel2    (inp_sel2_out),
                       .out_sel     (ex_sel_out),
                       .ecause      (ecause_out),
                       .prod        (prod_out),
                       .rs1_fw      (rs1_fw_out),
                       .rs2_fw      (rs2_fw_out),
                       .f3          (dec_f3_out),
                       .f7e         (dec_f7e_out),
                       .rd          (dec_rd_out));
  
  
  // EX Stage Modules

  // Input 1 Port Mux
  mux2_1x32 INP1_MUX  (.a     (s1_rf_ex),
                       .b     (pc_rf_ex),
                       .sel   (inp_sel1_rf_ex),
                       .out   (inp1_mux_out));

  // Input 2 Port Mux
  mux2_1x32 INP2_MUX  (.a     (s2_rf_ex),
                       .b     (imm_rf_ex),
                       .sel   (inp_sel2_rf_ex),
                       .out   (inp2_mux_out));

  // ALU
  kirv_alu ALU        (.a     (inp1_mux_out),
                       .b     (inp2_mux_out),
                       .f3    (f3_rf_ex),      // Main alu function
                       .f7e   (f7e_rf_ex),     // Controls cin, shift dir
                       .out   (alu_out));
  
  // Address Incrementor (by 4)
  inc4 INC4_EX        (.in    (pc_rf_ex),
                       .out   (pc4_rf_ex_out));

  // ALU Result Mux
  mux4_1x32 ALU_MUX   (.a     (alu_out),
                       .b     (imm_rf_ex),
                       .c     (pc4_rf_ex_out),
                       .d     (csr_res_rf_ex),
                       .sel   (ex_sel_rf_ex),
                       .out   (alu_mux_out));


  // Comparator (Branch unit)
  comp CMP            (.cp1   (s1_rf_ex),
                       .cp2   (s2_rf_ex),
                       .func  (f3_rf_ex),
                       .en    (cmp_op_rf_ex && val_rf_ex),
                       .unc   (jmp_op_rf_ex && val_rf_ex),
                       .res   (br_tak));

  // Dcache access generator (Currently does not check for unaligned access)
  dcache_acc_gen DAG  (.data_in       (s2_rf_ex),
                       .ld_op         (ld_op_rf_ex),
                       .st_op         (st_op_rf_ex),
                       .ls_type       (f3_rf_ex),
                       .addr_align_in (alu_out[1:0]),
                       .mask_out      (dag_mask_out),
                       .data_out      (dag_data_out));

  // CSR Logic Unit
  csr_lu CSR_LU       (.s1_in (s1_rf_ex),
                       .csr_in(csr_res_rf_ex),
                       .imm_in(imm_rf_ex[4:0]),     // Only uses 5 bits
                       .func  (f3_rf_ex),
                       .out   (csr_lu_out));
  
  
  // MA Stage Modules
  
  // dCache Data
  dcache       #(.FILE      ("data"))
         DCACHE (.clk_i     (clk_i),
                 .addr_0_i  (alu_mux_out),      // Equivalent to alu_ex_ma reg
                 .addr_1_i  (caddr_mux_out),    // Equivalent to caddr_ma_wb reg
                 .data_1_i  (cdata_mux_out),    // Equivalent to cdata_ma_wb reg
                 .byte_en_1_i (mask_ext_mux_out), // Equivalent to mask_ma_wb
                 .port_1_dir(cdir_mux_out),     // Equivalent to cdir_ma_wb
                 .stall_0   (stall_ex_ma),
                 .data_0_o  (dcache_data_out),  // LD data port 
                 .data_1_o  (dcache_data_o));   // External Data Port

  // LD Source Mux
  mux2_1x32 LD_MUX    (.a     (dcache_data_out),
                       .b     (ext_data_i),
                       .sel   (unc_sel_i),
                       .out   (ld_mux_out));

  // Data Load Controller (Sign extends/converts from little endian)
  data_ld_ctrl DATA_LD(.data_in (ld_mux_out),
                       .addr_in (alu_ex_ma),
                       .ctrl    (f3_ex_ma),
                       .data_out(data_ld_ctrl_out));
  
  // Result Mux
  mux2_1x32 RES_MUX   (.a     (alu_ex_ma),
                       .b     (data_ld_ctrl_out),
                       .sel   (ls_op_ex_ma),
                       .out   (res_mux_out));

  // rd commit Mux
  mux2_1x5 RD_COM_MUX (.a     (5'b0),
                       .b     (rd_ex_ma),
                       .sel   (commit),
                       .out   (rd_com_mux_out));

  // caddr Mux
  mux2_1x32 CADDR_MUX (.a     (alu_ex_ma),
                       .b     (ext_addr_i),
                       .sel   (ext_dcache_acc_i),
                       .out   (caddr_mux_out));

  // mask commit Mux
  mux2_1x4 MASK_COM_MUX(.a     (4'b0),
                       .b     (mask_ex_ma),
                       .sel   (commit && st_op_ex_ma && dtag_line_val),
                       .out   (mask_com_mux_out));

  // mask external Mux
  mux2_1x4 MASK_EXT_MUX(.a     (mask_com_mux_out),
                       .b     (ext_mask_i),
                       .sel   (ext_dcache_acc_i),
                       .out   (mask_ext_mux_out));

  // cdata Mux
  mux2_1x32 CDATA_MUX (.a     (cdata_ex_ma),
                       .b     (ext_data_i),
                       .sel   (ext_dcache_acc_i),
                       .out   (cdata_mux_out));

  // cdir Mux
  mux2_1 CDIR_MUX     (.a     (commit && st_op_ex_ma && dtag_line_val),
                       .b     (ext_dcache_dir_i),
                       .sel   (ext_dcache_acc_i),
                       .out   (cdir_mux_out));

  // cval Mux
  mux2_1 CVAL_MUX     (.a     (commit && st_op_ex_ma && dtag_line_val),
                       .b     (ext_dcache_val_i),
                       .sel   (ext_dcache_acc_i),
                       .out   (cval_mux_out));

  // cdty Mux
  mux2_1 CDTY_MUX     (.a     (commit && st_op_ex_ma && dtag_line_val),
                       .b     (ext_dcache_dty_i),
                       .sel   (ext_dcache_acc_i),
                       .out   (cdty_mux_out));

  // csr commit Mux
  mux2_1   CSR_COM_MUX(.a     (1'b0),
                       .b     (csr_op_ex_ma),
                       .sel   (commit),
                       .out   (csr_op_com_mux_out));

  // Selects which exception to take
  exc_arbiter EXC_ARB (.ecause_in (ecause_ex_ma),
                       .int_in    (int_ex_ma),  // Could derive interrupts from mip
                       .mie_in    (mie),
                       .mstatus_in(mstatus),
                       .priv      (priv),
                       .ecause_out(exc_arb_out),
                       .etak      (etak));

  // exception commit Mux
  mux2_1x3 EXC_COM_MUX(.a     (3'b0),
                       .b     (exc_arb_out),
                       .sel   (val_ex_ma && val_ma_wb),
                       .out   (ecause_com_mux_out));

  // dCache Tag
  l1dtag_cache #(.VFILE     ("l1d_val.dat"),
                 .TFILE     ("l1d_tag.dat"),
                 .DFILE     ("l1d_dty.dat"))
          DTAG  (.clk_i     (clk_i),
                 .rd_addr_i (alu_mux_out),        // Equivalent to alu_ex_ma
                 .wr_addr_i (caddr_mux_out),
                 .tag_i     (caddr_mux_out[31:12]),
                 .val_i     (cval_mux_out),
                 .dty_i     (cdty_mux_out),
                 .wr_en_i   (cdir_mux_out),
                 .we_rd_i   (1'b0),        // Change to wb_disabled for de2
                 .rd_stall_i(stall_ex_ma),
                 .tag_o     (dcache_tag_o),
                 .val_o     (dcache_val_out),
                 .dty_o     (dcache_dty_o));
   

  // Kludge added for DE2 Memory synthesis to force Dual Port MEM
  // we_disabled should always be 1'b0, but synthesizer is unable to optimize
  always @(*) begin
    
    if (stall_ex_ma && !stall_rf_ex) begin
      we_disabled = 1'b1;
    end else begin
      we_disabled = 1'b0;
    end

  end

   // Physical Memory Attribute check
   pma_cont PMA (.addr_i    (alu_ex_ma),
                 .val_o     (pma_val_out));

  // Commit signal logic
  assign commit = !(etak) && val_ma_wb && val_ex_ma;

  //
  assign dtag_line_val = (alu_ex_ma[31:12] == dcache_tag_o) && dcache_val_out;

  // MA Stall Request logic
  assign ma_stall_req = val_ex_ma && ls_op_ex_ma && !(etak) &&
                        !((dtag_line_val && pma_val_out) || unc_sel_i);

  // Uncacheable memory access request logic
  assign unc_acc_o = ls_op_ex_ma && !(pma_val_out) && !(etak) && val_ex_ma;

  // Assign external wires
  assign dcache_mask_o = mask_ex_ma;
  assign unc_data_o = cdata_ex_ma;
  assign dcache_dir_o = st_op_ex_ma;
  assign dcache_addr_o = alu_ex_ma;
  assign dcache_miss_o = ma_stall_req;
  
  
  // WB Stage Modules

  // Decodes the exception taken
  exc_dec EXC_DEC     (.ecause_in   (ecause_ma_wb),
                       .exc_tak     (exc_tak),
                       .ret_tak     (ret_tak));
  

  // Global Control Logic

  // Dispatch and forwarding logic
  dispatch_fw DP_FW  (.rs1_fw             (rs1_fw_out),
                      .rs2_fw             (rs2_fw_out),
                      .rs1_if_rf          (rs1_if_rf),
                      .rs2_if_rf          (rs2_if_rf),
                      .rd_rf_ex           (rd_rf_ex),
                      .rd_ex_ma           (rd_ex_ma),
                      .prod_rf_ex         (prod_rf_ex),
                      .prod_ex_ma         (prod_ex_ma),
                      .val_if_rf          (val_if_rf),
                      .val_rf_ex          (val_rf_ex),
                      .val_ex_ma          (val_ex_ma),
                      .fw_rs1_mux_sel     (fw_rs1_mux_sel),
                      .fw_rs2_mux_sel     (fw_rs2_mux_sel),
                      .dp_fw_stall_req    (dp_fw_stall_req));


  // Stall controller
  stall_ctrl ST_CTRL (.if_stall_req       (if_stall_req),
                      .dp_fw_stall_req    (dp_fw_stall_req),
                      .ma_stall_req       (ma_stall_req),
                      .ext_stall_req      (ext_stall_req_i),
                      .br_tak             (br_tak),
                      .exc_tak            (exc_tak),
                      .ret_tak            (ret_tak),
                      .stall_bp_if        (stall_bp_if),
                      .stall_if_rf        (stall_if_rf),
                      .stall_rf_ex        (stall_rf_ex),
                      .stall_ex_ma        (stall_ex_ma),
                      .val_if_rf          (val_if_rf_in),
                      .val_rf_ex          (val_rf_ex_in),
                      .val_ex_ma          (val_ex_ma_in),
                      .val_ma_wb          (val_ma_wb),
                      .icache_miss        (icache_miss_o));

  // Pipeline Register Transitions
  always @(posedge clk_i) begin
    
    // BP-IF Register Transitions
    if (!stall_bp_if) begin

      pc_bp_if <= pc_mux_out;

    end

    // IF-RF Register Transitions
    if (!stall_if_rf) begin
      
      pc_if_rf <= pc_bp_if;
      rs1_if_rf <= icache_data_out[`INST_RS1];
      rs2_if_rf <= icache_data_out[`INST_RS2];
      ir_if_rf <= icache_data_out;

      if (val_if_rf_in)
        val_if_rf <= 1'b1;
      else      
        val_if_rf <= 1'b0;

    end

    // RF-EX Register Transitions
    if (!stall_rf_ex) begin
      
      pc_rf_ex <= pc_if_rf;
      s1_rf_ex <= s1_out;
      s2_rf_ex <= s2_out;
      imm_rf_ex <= imm_out;
      csr_res_rf_ex <= csr_res_out;
      csr_dest_rf_ex <= csr_dest_out;
      csr_val_rf_ex <= csr_val_out;
      f3_rf_ex <= dec_f3_out;
      f7e_rf_ex <= dec_f7e_out;
      rd_rf_ex <= dec_rd_out;
      cmp_op_rf_ex <= cmp_op_out;
      jmp_op_rf_ex <= jmp_op_out;
      ld_op_rf_ex <= ld_op_out;
      st_op_rf_ex <= st_op_out;
      csr_op_rf_ex <= csr_op_out;
      inp_sel1_rf_ex <= inp_sel1_out;
      inp_sel2_rf_ex <= inp_sel2_out;
      ex_sel_rf_ex <= ex_sel_out;
      ecause_rf_ex <= ecause_out;
      prod_rf_ex <= prod_out;

      if (val_rf_ex_in)
        val_rf_ex <= val_if_rf;
      else      
        val_rf_ex <= 1'b0;

    end

    // EX-MA Register Transitions
    if (!stall_ex_ma) begin
      
      pc_ex_ma <= pc_rf_ex;
      alu_ex_ma <= alu_mux_out;
      mask_ex_ma <= dag_mask_out;
      f3_ex_ma <= f3_rf_ex;
      cdata_ex_ma <= dag_data_out;
      st_op_ex_ma <= st_op_rf_ex;
      rd_ex_ma <= rd_rf_ex;
      csr_dest_ex_ma <= csr_dest_rf_ex;
      csr_op_ex_ma <= csr_op_rf_ex;
      csr_res_ex_ma <= csr_lu_out;
      ecause_ex_ma <= ecause_rf_ex;
      prod_ex_ma <= prod_rf_ex;
      ls_op_ex_ma <= ld_op_rf_ex || st_op_rf_ex;    // Relocate in a module
      int_ex_ma <= int_i;

      if (val_ex_ma_in)
        val_ex_ma <= val_rf_ex;
      else      
        val_ex_ma <= 1'b0;

    end

    // MA-WB Register Transitions
    pc_ma_wb <= pc_ex_ma;
    res_ma_wb <= res_mux_out;
    rd_ma_wb <= rd_com_mux_out;
    caddr_ma_wb <= caddr_mux_out;
    mask_ma_wb <= mask_ext_mux_out;
    cdata_ma_wb <= cdata_mux_out;
    cdir_ma_wb <= cdir_mux_out;
    cval_ma_wb <= cval_mux_out;
    cdty_ma_wb <= cdty_mux_out;
    csr_res_ma_wb <= csr_res_ex_ma;
    csr_dest_ma_wb <= csr_dest_ex_ma;
    csr_op_ma_wb <= csr_op_com_mux_out;
    ecause_ma_wb <= ecause_com_mux_out;
    int_ma_wb <= int_ex_ma;

  end

  // Register Initialization
  initial begin

    // BP-IF Pipeline Registers
    pc_bp_if = 32'b0;

    // IF-RF Pipeline Registers
    pc_if_rf = 32'b0;
    rs1_if_rf = 5'b0;
    rs2_if_rf = 5'b0;
    ir_if_rf = 32'b0;         // op is currently not separated
    val_if_rf = 1'b0;         // Current instruction is valid

    // RF-EX Pipeline Registers
    pc_rf_ex = 32'b0;
    s1_rf_ex = 32'b0;
    s2_rf_ex = 32'b0;
    imm_rf_ex = 32'b0;
    csr_res_rf_ex = 32'b0;
    csr_dest_rf_ex = 3'b000;  // Encoded csr file address
    csr_val_rf_ex = 1'b0;
    f3_rf_ex = 3'b0;
    f7e_rf_ex = 2'b00;        // Encoded F7 field
    rd_rf_ex = 5'b0;
    cmp_op_rf_ex = 1'b0;      // Is a branch instruction
    jmp_op_rf_ex = 1'b0;      // Is a jump instruction
    ld_op_rf_ex = 1'b0;       // Is a load instruction
    st_op_rf_ex = 1'b0;       // Is a store instruction
    csr_op_rf_ex = 1'b0;      // Is a csr instruction
    inp_sel1_rf_ex = 1'b0;    // Selects ALU input (pc/register)
    inp_sel2_rf_ex = 1'b0;    // Selects ALU input (register/imm)
    ex_sel_rf_ex = 2'b00;     // Selects EX state result (alu/imm/pc+4/csr)
    ecause_rf_ex = 3'b000;    // Encodes call/ret/invalid instruction types
    prod_rf_ex = 2'b00;       // Encodes which stage the result is produced in
    val_rf_ex = 1'b0;

    // EX-MA Pipeline Registers
    pc_ex_ma = 32'b0;
    alu_ex_ma = 32'b0;
    mask_ex_ma = 4'b0;        // Data cache byte enable
    f3_ex_ma = 3'b0;          // Data cache RD port control bits
    cdata_ex_ma = 32'b0;      // Output of rs1 for store instruction
    st_op_ex_ma = 1'b0;       // Is a store instruction
    rd_ex_ma = 5'b0;
    csr_dest_ex_ma = 3'b0;
    csr_op_ex_ma = 1'b0;
    csr_res_ex_ma = 32'b0;    // Result of CSR Inst
    ecause_ex_ma = 3'b0;
    prod_ex_ma = 2'b0;
    val_ex_ma = 1'b0;
    ls_op_ex_ma = 1'b0;
    int_ex_ma = 2'b0;

    // MA-WB Pipeline Registers
    pc_ma_wb = 32'b0;
    res_ma_wb = 32'b0;     // Result to be stored in RF
    rd_ma_wb = 5'b0;
    caddr_ma_wb = 32'b0;   // Cache store address
    mask_ma_wb = 4'b0;     // WB data cache port byte enable
    cdata_ma_wb = 32'b0;   // WB data cache port data reg
    cdir_ma_wb = 1'b0;     // WB data cache port is either storing or loading
                           // (Possibly both in the future?-faster evict?)
    csr_res_ma_wb = 32'b0; // Result to be stored in the CSR RF
    csr_dest_ma_wb = 3'b0;
    csr_op_ma_wb = 1'b0;
    ecause_ma_wb = 3'b0;   // Encodes final exception cause
    int_ma_wb = 2'b0;

  end

endmodule
