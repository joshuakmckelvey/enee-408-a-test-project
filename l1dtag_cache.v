// L1 Tag Cache (256 lines) ( With Read New Value Forwarding)
module l1dtag_cache (
  
  input clk_i,
  input [31:0] rd_addr_i,  // Only 10 bits are used [13:4]
  input [31:0] wr_addr_i,  // Only 10 bits are used [13:4]
  input [19:0] tag_i,   // 20 Addr bits
  input val_i,          // Valid bit
  input dty_i,          // Dirty bit

  input wr_en_i,
  input we_rd_i,

  input rd_stall_i,

  output reg [19:0] tag_o,
  output reg val_o,
  output reg dty_o

);
  
  parameter TFILE = "";   // Tag init file
  parameter VFILE = "";   // Val init file
  parameter DFILE = "";   // Dty init file

  wire val_mem;
  wire dty_mem;
  wire [19:0] tag_mem;

  reg val_i_reg;
  reg dty_i_reg;
  reg [19:0] tag_i_reg;
  reg fwd;

  reg [31:0] rd_addr;
  reg [31:0] rd_addr_buf;

  mem_2rw_stall_param #(.FILE     (TFILE),
                        .ADDR_WIDTH (12-4),
                        .DATA_WIDTH (20))
                   TAGM(.clock    (clk_i),
                        .data_a   (tag_i),
                        .data_b   (tag_i),
                        .address_a(rd_addr_i[11:4]),
                        .address_b(wr_addr_i[11:4]),
                        .addressstall_a(rd_stall_i),
                        .wren_a   (we_rd_i),
                        .wren_b   (wr_en_i),
                        .q_a      (tag_mem),
                        .q_b      ());

  mem_2rw_stall_param #(.FILE     (VFILE),
                        .ADDR_WIDTH (12-4),
                        .DATA_WIDTH (1))
                   VALM(.clock    (clk_i),
                        .data_a   (val_i),
                        .data_b   (val_i),
                        .address_a(rd_addr_i[11:4]),
                        .address_b(wr_addr_i[11:4]),
                        .addressstall_a(rd_stall_i),
                        .wren_a   (we_rd_i),
                        .wren_b   (wr_en_i),
                        .q_a      (val_mem),
                        .q_b      ());

  mem_2rw_stall_param #(.FILE     (DFILE),
                        .ADDR_WIDTH (12-4),
                        .DATA_WIDTH (1))
                   DTYM(.clock    (clk_i),
                        .data_a   (dty_i),
                        .data_b   (dty_i),
                        .address_a(rd_addr_i[11:4]),
                        .address_b(wr_addr_i[11:4]),
                        .addressstall_a(rd_stall_i),
                        .wren_a   (we_rd_i),
                        .wren_b   (wr_en_i),
                        .q_a      (dty_mem),
                        .q_b      ());

  // RD address stall buffer
  always @(*) begin
    
    if (rd_stall_i)
      rd_addr = rd_addr_buf;
    else
      rd_addr = rd_addr_i;

  end

  always @ (posedge clk_i) begin
    
    if (!rd_stall_i)
      rd_addr_buf <= rd_addr_i;

  end

  // WR->RD data forwarding detector
  always @(posedge clk_i) begin

    if (wr_en_i && (rd_addr[13:4] == wr_addr_i[13:4])) begin
      fwd <= 1'b1;
    end else begin
      fwd <= 1'b0;
    end

    val_i_reg <= val_i;
    tag_i_reg <= tag_i;
    dty_i_reg <= dty_i;

  end

  // WR->RD data forwarding
  always @(*) begin

    if (fwd) begin
      val_o = val_i_reg;
      tag_o = tag_i_reg;
      dty_o = dty_i_reg;
    end else begin
      val_o = val_mem;
      tag_o = tag_mem;
      dty_o = dty_mem;
    end

  end

endmodule
