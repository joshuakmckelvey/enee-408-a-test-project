// Data Load Controller
module data_ld_ctrl (data_in, addr_in, ctrl, data_out);

  input [31:0] addr_in;
  input [31:0] data_in;

  input [2:0] ctrl;  // Load Control (F3)

  output reg [31:0] data_out;


  // Reorders Loads (& Sign extends/Zero extends)
  always @(*) begin

    // Low order bits selection
    case (addr_in[1:0])
      2'b00:  data_out[7:0] = data_in[31:24];
      2'b01:  data_out[7:0] = data_in[23:16];
      2'b10:  data_out[7:0] = data_in[15:8];
      2'b11:  data_out[7:0] = data_in[7:0];
      default: data_out[7:0] = data_in[31:24]; // Default is not needed
    endcase

    // Second lowest order bits selection
    if (ctrl[1:0] == 2'b00) begin      // ctrl is LB(U)
      if (ctrl[2] == 1'b0) begin       // ctrl is signed
        data_out[15:8] = {8{data_out[7]}};  // sign extends
      end else
        data_out[15:8] = 8'b0;    // zero extends
      end
    else begin
      if (addr_in[1] == 1'b0)
        data_out[15:8] = data_in[23:16];
      else
        data_out[15:8] = data_in[7:0];
    end

    // Higher order bits selection
    if (ctrl[1] == 1'b1) begin         // ctrl is LW
      data_out[23:16] = data_in[15:8];
      data_out[31:24] = data_in[7:0];
    end else begin
      if (ctrl[2] == 1'b0)   // ctrl is signed
        if (ctrl[0] == 1'b0) // ctrl is LB
          data_out[31:16] = {16{data_out[7]}};
        else  // ctrl is LW
          data_out[31:16] = {16{data_out[15]}};
      else
        data_out[31:16] = 16'b0;
    end

  end

endmodule
