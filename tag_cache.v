// Tag Cache (256 lines)
module tag_cache (clk_i, rd_addr_i, wr_addr_i, tag_i, val_i, wr_en_i, rd_stall_i,
                  tag_o, val_o);
  
  input clk_i;
  input [31:0] rd_addr_i;  // Only 8 bits are used [11:4]
  input [31:0] wr_addr_i;  // Only 8 bits are used [11:4]
  input [19:0] tag_i;   // 20 Addr bits
  input val_i;          // Valid bit

  input wr_en_i;
  input rd_stall_i;

  output [19:0] tag_o;
  output val_o;
  
  parameter TFILE = "";
  parameter VFILE = "";

  //reg vmem [0:255];
  reg [7:0] read_addr;

  initial begin

    if (VFILE != "" && TFILE != "") begin
      //$readmemb({VFILE, ".dat"}, vmem);
      //$readmemh({TFILE, ".dat"}, tmem);
    end
    //tag_o <= 20'b0;
    //val_o <= 1'b0;
    //read_addr <= 8'b0;

  end


  mem_1w_1r_stall_param #(.FILE     ({TFILE, ".dat"}),
                        .ADDR_WIDTH (8),
                        .DATA_WIDTH (20))
                   TAGM(.clock    (clk_i),
                        .rd_addressstall  (rd_stall_i),
                        .data     (tag_i),
                        .rdaddress(rd_addr_i[11:4]),
                        .wraddress(wr_addr_i[11:4]),
                        .wren     (wr_en_i),
                        .q        (tag_o));

  mem_1w_1r_stall_param #(.FILE     ({VFILE, ".dat"}),
                        .ADDR_WIDTH (8),
                        .DATA_WIDTH (1))
                   VALM(.clock    (clk_i),
                        .rd_addressstall  (rd_stall_i),
                        .data     (val_i),
                        .rdaddress(rd_addr_i[11:4]),
                        .wraddress(wr_addr_i[11:4]),
                        .wren     (wr_en_i),
                        .q        (val_o));

/*
  always @(posedge clk_i) begin

    if (wr_en_i)
      vmem[addr_i[11:4]] <= val_i;

    if (!rd_stall_i)
    read_addr <= addr_i[11:4];
    //val_o = vmem[addr_i[11:4]];   // With forwarding

  end

  assign val_o = vmem[read_addr];
  */

endmodule
