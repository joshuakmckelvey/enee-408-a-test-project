`define F3_CSRRW    3'b001
`define F3_CSRRS    3'b010
`define F3_CSRRC    3'b011
`define F3_CSRRWI   3'b101
`define F3_CSRRSI   3'b110
`define F3_CSRRCI   3'b111

// CSR Logic unit
module csr_lu (s1_in, csr_in, imm_in, func, out);

  input [31:0] s1_in, csr_in;
  input [4:0] imm_in;         // Unsigned
  input [2:0] func;

  output reg [31:0] out;

  always @(*) begin
    case ( func )
        `F3_CSRRW:    out = s1_in;
        `F3_CSRRS:    out = s1_in | csr_in;
        `F3_CSRRC:    out = (~s1_in) & csr_in;
        `F3_CSRRWI:   out = ({27'b0, imm_in});
        `F3_CSRRSI:   out = ({27'b0, imm_in}) | csr_in;
        `F3_CSRRCI:   out = ({27'b0, imm_in}) & (~csr_in);
        default:      out = s1_in;    // Don't Care
    endcase
  end

endmodule
