// Exception/Interrupt Decoder

`define ECAUSE_NONE   3'b000
`define ECAUSE_ECALL  3'b001
`define ECAUSE_MRET   3'b010
`define ECAUSE_MTIME  3'b100
`define ECAUSE_MEXT   3'b101
`define ECAUSE_INV    3'b111

module exc_dec (ecause_in, exc_tak, ret_tak);

  input [2:0] ecause_in;

  output reg exc_tak;
  output reg ret_tak;

  always @(*) begin
    
    case (ecause_in)
      `ECAUSE_NONE: begin
        exc_tak = 1'b0;
        ret_tak = 1'b0;
      end
      `ECAUSE_ECALL: begin
        exc_tak = 1'b1;
        ret_tak = 1'b0;
      end
      `ECAUSE_MRET: begin
        exc_tak = 1'b0;
        ret_tak = 1'b1;
      end
      `ECAUSE_MTIME: begin
        exc_tak = 1'b1;
        ret_tak = 1'b0;
      end
      `ECAUSE_MEXT: begin
        exc_tak = 1'b1;
        ret_tak = 1'b0;
      end
      `ECAUSE_INV: begin
        exc_tak = 1'b1;
        ret_tak = 1'b0;
      end
      default: begin
        exc_tak = 1'b1;
        ret_tak = 1'b0;
      end

    endcase

  end

endmodule
