// PMA Checker (MSB determines valid memory vs. uncacheable region)
module pma_cont (addr_i, val_o);
  
  input [31:0] addr_i;

  output val_o;
  
  assign val_o = !addr_i[31];

endmodule
