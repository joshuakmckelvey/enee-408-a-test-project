// 32 bit X 32 register file, 1 Write port, 2 Read ports
module rf32x32_1w_2r (clk, wr_port, wr_en, wr_sel, rd_stall, rd_a_sel, rd_b_sel,
                      rd_a_port, rd_b_port);
  
  input clk, wr_en;
  input [31:0] wr_port;

  input [4:0] wr_sel, rd_a_sel, rd_b_sel;

  input rd_stall;

  output reg [31:0] rd_a_port, rd_b_port;

  wire [31:0] rd_a_port_buf, rd_b_port_buf;

  reg [4:0] wr_sel_buf, rd_a_sel_buf, rd_b_sel_buf;
  reg [31:0] wr_port_buf;

  mem_1w_1r_stall_param #(.FILE     (),
                        .ADDR_WIDTH (5),
                        .DATA_WIDTH (32))
                  RF_P0(.clock      (clk),
                        .rd_addressstall  (rd_stall),
                        .data       (wr_port),
                        .rdaddress  (rd_a_sel),
                        .wraddress  (wr_sel),
                        .wren       (wr_en),
                        .q          (rd_a_port_buf));

  mem_1w_1r_stall_param #(.FILE     (),
                        .ADDR_WIDTH (5),
                        .DATA_WIDTH (32))
                  RF_P1(.clock      (clk),
                        .rd_addressstall  (rd_stall),
                        .data       (wr_port),
                        .rdaddress  (rd_b_sel),
                        .wraddress  (wr_sel),
                        .wren       (wr_en),
                        .q          (rd_b_port_buf));

  // Forwarding registers
  always @(posedge clk) begin

    wr_sel_buf <= wr_sel;
    wr_port_buf <= wr_port;

    if (!rd_stall) begin
      rd_a_sel_buf <= rd_a_sel;
      rd_b_sel_buf <= rd_b_sel;
    end

  end

  // Read port and write forwarding logic
  always @ (*) begin
    
    if (rd_a_sel_buf == 5'b00000)
        rd_a_port = 32'h00000000;
    else if (rd_a_sel_buf == wr_sel_buf)
        rd_a_port = wr_port_buf;
    else
        rd_a_port = rd_a_port_buf;
    
    if (rd_b_sel_buf == 5'b00000)
        rd_b_port = 32'h00000000;
    else if (rd_b_sel_buf == wr_sel_buf)
        rd_b_port = wr_port_buf;
    else
        rd_b_port = rd_b_port_buf;
    
  end

endmodule
