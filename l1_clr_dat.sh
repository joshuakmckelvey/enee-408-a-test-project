#!/bin/bash
CACHE_LINES=256

rm -f l1i_tag.dat
rm -f l1i_val.dat
rm -f l1i_dty.dat

rm -f l1d_tag.dat
rm -f l1d_val.dat
rm -f l1d_dty.dat

for ((TMP=0; TMP<$CACHE_LINES; TMP++))
do
    echo "00000" >> l1i_tag.dat
    echo "0" >> l1i_val.dat
    echo "0" >> l1i_dty.dat

    echo "00000" >> l1d_tag.dat
    echo "0" >> l1d_val.dat
    echo "0" >> l1d_dty.dat

done
