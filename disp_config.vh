`ifndef _disp_config_vh_
`define _disp_config_vh_

`define RES160X120AT60_25M

// 100x75@60hz 10MHz Clock
`ifdef RES100X75AT60_10M

  `define H_ACTIVE      9'd200 // Horiz. Active time (Display Area)
  `define H_FPORTCH     9'd210 // Horiz. Front portch (end)
  `define H_SYNC        9'd242 // Sync pulse start
  `define H_BPORTCH     9'd264 // Back Portch

  `define V_ACTIVE      10'd600
  `define V_FPORTCH     10'd601
  `define V_SYNC        10'd605
  `define V_BPORTCH     10'd628

  `define  H_BITS       9
  `define  V_BITS       10

  `define  H_BITS_ACT   8
  `define  V_BITS_ACT   10 // Needs adjustment after fixing fb access

  `define  H_BITS_TRUNC 1
  `define  V_BITS_TRUNC 3

`endif

// 100x75@60hz 40MHz Clock
`ifdef RES100X75AT60_40M

  `define H_ACTIVE      11'd800   // Horiz. Active time (Display Area)
  `define H_FPORTCH     11'd843   // Horiz. Front portch (end)
  `define H_SYNC        11'd971   // Sync pulse start
  `define H_BPORTCH     11'd1056  // Back Portch

  `define V_ACTIVE      10'd600
  `define V_FPORTCH     10'd601
  `define V_SYNC        10'd605
  `define V_BPORTCH     10'd628

  `define  H_BITS       11
  `define  V_BITS       10

  `define  H_BITS_ACT   10
  `define  V_BITS_ACT   10

  `define  H_BITS_TRUNC 3
  `define  V_BITS_TRUNC 3

`endif

// 800x600@60hz 40MHz Clock
`ifdef RES800X600AT60_40M

  `define H_ACTIVE      11'd800   // Horiz. Active time (Display Area)
  `define H_FPORTCH     11'd843   // Horiz. Front portch (end)
  `define H_SYNC        11'd971   // Sync pulse start
  `define H_BPORTCH     11'd1056  // Back Portch

  `define V_ACTIVE      10'd600
  `define V_FPORTCH     10'd601
  `define V_SYNC        10'd605
  `define V_BPORTCH     10'd628

  `define  H_BITS       11
  `define  V_BITS       10

  `define  H_BITS_ACT   10
  `define  V_BITS_ACT   10

  `define  H_BITS_TRUNC 0
  `define  V_BITS_TRUNC 0

`endif

// 160x120@60hz 25.175MHz Clock
`ifdef RES160X120AT60_25M

  `define H_ACTIVE      10'd640   // Horiz. Active time (Display Area)
  `define H_FPORTCH     10'd659   // Horiz. Front portch (end)
  `define H_SYNC        10'd755   // Sync pulse start
  `define H_BPORTCH     10'd800   // Back Portch

  `define V_ACTIVE      10'd480
  `define V_FPORTCH     10'd493
  `define V_SYNC        10'd495
  `define V_BPORTCH     10'd525

  `define  H_BITS       10
  `define  V_BITS       10

  `define  H_BITS_ACT   10
  `define  V_BITS_ACT   9

  `define  H_BITS_TRUNC 2
  `define  V_BITS_TRUNC 2

`endif

// 640x480@60hz 25.175MHz Clock
`ifdef RES640X480AT60_25M

  `define H_ACTIVE      10'd640   // Horiz. Active time (Display Area)
  `define H_FPORTCH     10'd659   // Horiz. Front portch (end)
  `define H_SYNC        10'd755   // Sync pulse start
  `define H_BPORTCH     10'd800   // Back Portch

  `define V_ACTIVE      10'd480
  `define V_FPORTCH     10'd493
  `define V_SYNC        10'd495
  `define V_BPORTCH     10'd525

  `define  H_BITS       10
  `define  V_BITS       10

  `define  H_BITS_ACT   10
  `define  V_BITS_ACT   9

  `define  H_BITS_TRUNC 0
  `define  V_BITS_TRUNC 0

`endif

`endif
