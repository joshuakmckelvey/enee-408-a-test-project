// Immediate decoder

// Definitions for Major Processor Opcodes
`define LOAD        5'b00000
`define OP_IMM      5'b00100
`define AUIPC       5'b00101
`define STORE       5'b01000
`define LUI         5'b01101
`define BRANCH      5'b11000
`define JALR        5'b11001
`define JAL         5'b11011
`define SYSTEM      5'b11100

// Immediate bit fields
`define IMM_ISBJ_10_5   30:25
`define IMM_IS_11       31
`define IMM_IJ_4_1      24:21
`define IMM_I_0         20
`define IMM_SB_4_1      11:8
`define IMM_S_0         7
`define IMM_B_11        7
`define IMM_B_12        31
`define IMM_UJ_19_12    19:12
`define IMM_U_30_20     30:20
`define IMM_J_11        20
`define IMM_J_10_1      30:21
`define IMM_J_20        31

`define IMM_MSB     31  // Immediate's sign bit

`define INST_UIMM   19:15 // Unsigned immediate (for CSR Instructions)

module imm_decode (in, sel, out);

  input [31:0] in;
  input [4:0] sel;

  output reg [31:0] out;
  

  wire [31:0] imm_i = { {21{in[`IMM_MSB]}}, in[`IMM_ISBJ_10_5],
                      in[`IMM_IJ_4_1], in[`IMM_I_0] };
  wire [31:0] imm_s = { {21{in[`IMM_MSB]}}, in[`IMM_ISBJ_10_5],
                      in[`IMM_SB_4_1], in[`IMM_S_0] };
  wire [31:0] imm_b = { {20{in[`IMM_MSB]}}, in[`IMM_B_11],
                      in[`IMM_ISBJ_10_5], in[`IMM_SB_4_1], 1'b0 };
  wire [31:0] imm_u = { in[`IMM_MSB], in[`IMM_U_30_20],
                      in[`IMM_UJ_19_12], 12'b0 };
  wire [31:0] imm_j = { {12{in[`IMM_MSB]}}, in[`IMM_UJ_19_12],
                      in[`IMM_J_11], in[`IMM_ISBJ_10_5],
                      in[`IMM_IJ_4_1], 1'b0 };

  // CSR Immediate
  wire [31:0] imm_c = {28'b0, in[`INST_UIMM]};  // Could steer unused bits
                                                // from MSB if efficient

  always @(*) begin
    case ( sel )
      `LUI:     out = imm_u;
      `AUIPC:   out = imm_u;
      `JAL:     out = imm_j;
      `JALR:    out = imm_i;
      `BRANCH:  out = imm_b;
      `LOAD:    out = imm_i;
      `STORE:   out = imm_s;
      `OP_IMM:  out = imm_i;
      `SYSTEM:  out = imm_c;
      default:  out = imm_i; // Arbitrary defaualt
    endcase
  end

endmodule
