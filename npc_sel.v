// Next PC selector
module npc_sel (branch, exc, ret, sel);
    
    input branch, exc, ret;

    output reg [1:0] sel;

    always @(*) begin
        
        if (exc)
          sel <= 2'b10;
        else if (ret)
          sel <= 2'b11;
        else if (branch)
          sel <= 2'b01;
        else
          sel <= 2'b00;

    end

endmodule
