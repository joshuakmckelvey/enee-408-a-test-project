# Output Program ELF Exec
PROG=mandel
# Input C Program Directory
PROGDIR=tests/mandel
# Input System/Sycall Directory
SYSDIR=tests/system
# Verilog Testbench
TESTB=kirv_soc_tb
MARCH= -march=rv32i -mabi=ilp32
COPS = -static -nostdlib -nostartfiles -ffreestanding -s -Qn -O3
CC=riscv32-unknown-linux-gnu-gcc
LINKERFILE=linker_l2.ld
CLOCKRATE=10000000

# Address bits in L2 Cache (14=16KB)
L2_BITS=15
L2_TAGBITS=$(shell echo $$((32-$(L2_BITS))))

C_DEFINES=-DPERFORMANCE_RUN=1 -DITERATIONS=200 -DCLOCKRATE=$(CLOCKRATE) -DFLAGS_STR='"$(COPS)"'

C_SOURCES=$(wildcard $(PROGDIR)/*.c)
C_OBJECTS=$(patsubst $(PROGDIR)/%.c, $(PROGDIR)/%.o, $(C_SOURCES))

HEADERS=$(wildcard $(PROGDIR)/*.h)

# Assembly files must start with system.S for correct linking
S_SOURCES=$(SYSDIR)/system.S $(SYSDIR)/syscall.S $(SYSDIR)/div.S $(SYSDIR)/muldi3.S
S_OBJECTS=$(patsubst %.S, %.o, $(S_SOURCES))

all : $(TESTB).vvp $(TESTB).vcd

$(TESTB).vvp : program
	iverilog -Wall -o $(TESTB).vvp -c modules.txt

$(TESTB).vcd : $(TESTB).vvp
	vvp $(TESTB).vvp

# Program is loaded to L2 cache
program : $(PROG).x
	elfconv --infile $(PROGDIR)/$(PROG).x --split 0 --stride 1 --wordlen 4 --tagbits $(L2_TAGBITS) --outfile l2_init
	touch mem_0.dat mem_1.dat mem_2.dat mem_3.dat
	touch text_0.dat text_1.dat text_2.dat text_3.dat
	touch data_0.dat data_1.dat data_2.dat data_3.dat
	bash ./l1_clr_dat.sh
	cp *.dat de0_modules/
	cp *.dat de2_modules/
	cp *.dat gen2_modules/
	cp *.dat de2_115_modules/
	cp *.dat arty_a7_modules/

# Program is loaded to Mem
#program : $(PROG).x
#	elfconv --infile $(PROGDIR)/$(PROG).x --split 0 --stride 4 --wordlen 1 --tagbits $(L2_TAGBITS) --outfile mem

# Program Linking
$(PROG).x : $(C_OBJECTS) $(S_OBJECTS) $(SYSDIR)/$(LINKERFILE)
	$(CC) $(MARCH) $(COPS) -T $(SYSDIR)/$(LINKERFILE) $(S_OBJECTS) $(C_OBJECTS) -o $(PROGDIR)/$(PROG).x

# C compilation
%.o : %.c $(HEADERS)
	$(CC) $(MARCH) $(COPS) -c $< $(C_DEFINES)
	mv *.o ./$(PROGDIR)

# Assembly compilation
%.o : %.S
	$(CC) $(MARCH) $(COPS) -c $<
	mv *.o ./$(SYSDIR)

clean :
	rm -f *.vvp *.vcd $(SYSDIR)/*.o $(PROGDIR)/*.o $(PROGDIR)/*.s $(PROGDIR)/*.x *.dat de0_modules/*.dat de2_modules/*.dat
