`define MIE     3     // Machine Interrupt Enable
`define MPIE    7     // Machine Previous Interrupt Enable
`define MPP     12:11 // Machine Previous Privilege

// mstatus register
module mstatus_reg (clk, din, priv, ld, exc_tak, ret_tak, dout);

  input [31:0] din;
  input ld, clk, exc_tak, ret_tak;
  input [1:0] priv;
  
  output [31:0] dout;

  reg mie, mpie;
  reg [1:0] mpp;

  initial begin
    mie <= 1'b0;
    mpie <= 1'b0;
    mpp <= 2'b00;
  end

  always @ (posedge clk) begin

    if (exc_tak) begin
      mie <= 1'b0;
      mpie <= mie;
      mpp <= priv;
    end else if (ret_tak) begin
      mie <= mpie;
      mpie <= 1'b1;
      mpp <= 2'b0;
    end else if (ld) begin
      mie <= din[`MIE];
      mpie <= din[`MPIE];
      mpp <= din[`MPP];
    end
  
  end

  assign dout[`MIE] = mie;
  assign dout[`MPIE] = mpie;
  assign dout[`MPP] = mpp;

  assign dout[31:13] = 19'b0;
  assign dout[10:8] = 3'b0;
  assign dout[6:4] = 3'b0;
  assign dout[2:0] = 3'b0;

endmodule
