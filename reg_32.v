// 32 bit register, initialized to zero
module reg_32 (clk, din, ld, dout);

  input [31:0] din;
  input ld, clk;
  
  output reg [31:0] dout;

  initial begin

    dout <= 32'b0;

  end

  always @ (posedge clk) begin

    if (ld)
      dout <= din;
  
  end

endmodule
