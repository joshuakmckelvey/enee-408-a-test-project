// Comparator (Branch unit)

`define F3_EQ   3'b000
`define F3_NE   3'b001
`define F3_LT   3'b100
`define F3_GE   3'b101
`define F3_LTU  3'b110
`define F3_GEU  3'b111

module comp (cp1, cp2, func, en, unc, res);
  
  input [31:0] cp1, cp2;
  input [2:0] func;
  input en;
  input unc; // Unconditional (Ex. JMP)

  output reg res;

  always @(*) begin
  
    if (unc) begin

      res <= 1;

    end else if (en) begin
        case ( func )
          `F3_EQ: if (cp1 == cp2) begin
                      res <= 1;
                  end else begin
                      res <= 0;
                  end
          `F3_NE: if (cp1 != cp2) begin
                      res <= 1;
                  end else begin
                      res <= 0;
                  end
          `F3_LT: if ($signed(cp1) < $signed(cp2)) begin
                      res <= 1;
                  end else begin
                      res <= 0;
                  end
          `F3_GE: if ($signed(cp1) >= $signed(cp2)) begin
                      res <= 1;
                  end else begin
                      res <= 0;
                  end
          `F3_LTU:if (cp1 < cp2) begin
                      res <= 1;
                  end else begin
                      res <= 0;
                  end
          `F3_GEU:if (cp1 >= cp2) begin
                      res <= 1;
                  end else begin
                      res <= 0;
                  end
          default: begin res <= 0; end
        endcase
    end else begin
          res <= 0;
    end

  end


endmodule
