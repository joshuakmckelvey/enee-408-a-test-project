# Setup:

This project requires the RISC-V GCC toolchain to be installed, found at this link: [https://github.com/riscv-collab/riscv-gnu-toolchain](https://github.com/riscv-collab/riscv-gnu-toolchain)

The generic "riscv32-unknown-linux-gnu" version of the compiler toolchain is the correct option to install.

The other requirement for this project is the elfconv project, found here: [https://gitlab.com/joshuakmckelvey/elfconv](https://gitlab.com/joshuakmckelvey/elfconv)

Note: The toolchain executables and elfconv.x files must be added to path for UNIX machines.

# Project Use:

To compile the test programs and generate the corresponding memory initialization files, run: make program

To run the icarus verilog simulation, run: make

The DE0-Nano FPGA board's Quartus Prime Lite project is located in: de0_modules/reference_core.qpf
The DE2 FPGA board's Quartus II project is located in: de2_modules/reference_core.qpf
