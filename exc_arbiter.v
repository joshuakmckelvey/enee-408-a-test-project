// Exception/Interrupt Arbiter

`define MTIE    7   // Timer interrupt enabled bit
`define MEIE    11  // External interrupt enabled bit
`define MIE     3   // Global Interrupt Enable bit in mstatus

`define ECAUSE_NONE   3'b000
`define ECAUSE_ECALL  3'b001
`define ECAUSE_MRET   3'b010
`define ECAUSE_MTIME  3'b100
`define ECAUSE_MEXT   3'b101
`define ECAUSE_INV    3'b111

module exc_arbiter (ecause_in, int_in, mie_in, mstatus_in, priv,
                    ecause_out, etak);

  input [2:0] ecause_in;
  input [1:0] int_in;
  input [31:0] mie_in;
  input [31:0] mstatus_in;
  input [1:0] priv;

  output reg [2:0] ecause_out;
  output reg etak;

  always @(*) begin
    
    // Machine Timer Interrupt
    if ((int_in[0] && mie_in[`MTIE] && mstatus_in[`MIE]) ||   
        (int_in[0] && mie_in[`MTIE] && priv != 2'b11)) begin

      ecause_out = `ECAUSE_MTIME;
      etak = 1'b1;

    // Machine External Interrupt
    end else if ((int_in[1] && mie_in[`MEIE] && mstatus_in[`MIE]) || 
                 (int_in[1] && mie_in[`MEIE] && priv != 2'b11)) begin

      ecause_out = `ECAUSE_MEXT;
      etak = 1'b1;

    end else case (ecause_in)
      `ECAUSE_NONE: begin
        ecause_out = `ECAUSE_NONE;
        etak = 1'b0;
      end
      `ECAUSE_ECALL: begin
        ecause_out = `ECAUSE_ECALL;
        etak = 1'b1;
      end
      `ECAUSE_MRET: begin
        ecause_out = `ECAUSE_MRET;
        etak = 1'b1;
      end
      `ECAUSE_INV: begin
        ecause_out = `ECAUSE_INV;
        etak = 1'b1;
      end
      default: begin
        ecause_out = `ECAUSE_INV;
        etak = 1'b1;
      end

    endcase

  end

endmodule
