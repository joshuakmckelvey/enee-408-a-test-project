// Stall and squash controller

module stall_ctrl (if_stall_req, dp_fw_stall_req, ma_stall_req, ext_stall_req,
                   br_tak, exc_tak, ret_tak, stall_bp_if, stall_if_rf,
                   stall_rf_ex, stall_ex_ma, val_if_rf, val_rf_ex, val_ex_ma,
                   val_ma_wb, icache_miss);

  input if_stall_req, dp_fw_stall_req, ma_stall_req, ext_stall_req;
  input br_tak, exc_tak, ret_tak;

  output reg stall_bp_if, stall_if_rf, stall_rf_ex, stall_ex_ma;
  output reg val_if_rf, val_rf_ex, val_ex_ma, val_ma_wb;
  output reg icache_miss;


  always @(*) begin
    
    if (ext_stall_req) begin
      val_if_rf = 1'b1;
      val_rf_ex = 1'b1;
      val_ex_ma = 1'b1;
      val_ma_wb = 1'b0;
      stall_bp_if = 1'b1;
      stall_if_rf = 1'b1;
      stall_rf_ex = 1'b1;
      stall_ex_ma = 1'b1;
      icache_miss = 1'b0;
    end else if (exc_tak || ret_tak) begin
      val_if_rf = 1'b0;
      val_rf_ex = 1'b0;
      val_ex_ma = 1'b0;
      val_ma_wb = 1'b0;
      stall_bp_if = 1'b0;
      stall_if_rf = 1'b0;
      stall_rf_ex = 1'b0;
      stall_ex_ma = 1'b0;
      icache_miss = 1'b0;
    end else if (if_stall_req) begin
      val_if_rf = 1'b1;
      val_rf_ex = 1'b1;
      val_ex_ma = 1'b1;
      val_ma_wb = 1'b0;
      stall_bp_if = 1'b1;
      stall_if_rf = 1'b1;
      stall_rf_ex = 1'b1;
      stall_ex_ma = 1'b1;
      icache_miss = 1'b1;
    end else if (ma_stall_req) begin
      val_if_rf = 1'b1;
      val_rf_ex = 1'b1;
      val_ex_ma = 1'b1;
      val_ma_wb = 1'b0;
      stall_bp_if = 1'b1;
      stall_if_rf = 1'b1;
      stall_rf_ex = 1'b1;
      stall_ex_ma = 1'b1;
      icache_miss = 1'b0;
    end else if (br_tak) begin
      val_if_rf = 1'b0;
      val_rf_ex = 1'b0;
      val_ex_ma = 1'b1;
      val_ma_wb = 1'b1;
      stall_bp_if = 1'b0;
      stall_if_rf = 1'b0;
      stall_rf_ex = 1'b0;
      stall_ex_ma = 1'b0;
      icache_miss = 1'b0;
    end else if (dp_fw_stall_req) begin
      val_if_rf = 1'b1;
      val_rf_ex = 1'b0;
      val_ex_ma = 1'b1;
      val_ma_wb = 1'b1;
      stall_bp_if = 1'b1;
      stall_if_rf = 1'b1;
      stall_rf_ex = 1'b0;
      stall_ex_ma = 1'b0;
      icache_miss = 1'b0;
    end else begin
      val_if_rf = 1'b1;
      val_rf_ex = 1'b1;
      val_ex_ma = 1'b1;
      val_ma_wb = 1'b1;
      stall_bp_if = 1'b0;
      stall_if_rf = 1'b0;
      stall_rf_ex = 1'b0;
      stall_ex_ma = 1'b0;
      icache_miss = 1'b0;
    end

  end

endmodule
